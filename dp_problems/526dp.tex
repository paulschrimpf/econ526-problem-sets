\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{mdframed}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{newpxtext}
\usepackage{newpxmath}
%\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage{ulem}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 
\def\R{\mathbb{R}}
\def\Er{\mathrm{E}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}

\newtheorem{prob}{Problem}
\newtheorem{solution}{Solution to problem}
\newenvironment{problem}
  {\begin{mdframed}\begin{prob}}
      {\end{prob}\end{mdframed}}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\title{Dynamic Programming Exercises}
%\date{}

\begin{document}

\maketitle

These all the problems related to dynamic programming that have
appeared on past exams. 

\begin{problem}[\label{wage}]
  This is from the 2011 final. The order of topics in the course was
  different back then.
  
  Suppose an unemployed worker receives a wage offer independently
  distributed uniformly on $[0,\bar{w}]$ each period. If the worker
  accepts the offer, the worker receives the wage forever. If the
  worker declines the offer, the worker receives unemployment benefits
  of $b$ and continues to be unemployed next period. The worker
  discounts the future at rate $\beta$, and has utility equal to wages
  or unemployment benefits. 
  \begin{enumerate}
  \item Find the utility from accepting a wage of $w$. \textit{Hint: this
      is short.}
  \item Write the Bellman equation for the value of receiving a wage
    offer of $w$. \\
    \textit{Hint: There are multiple ways to set this up. One way is
      to let $V(w)$ be the value of getting an offer of $w$. Given an
      offer of $w$, you can either accept it and 
      get your answer from the previous part, or reject it and get $b$
      plus the discount factor times the expectation of $V(w)$ next
      period, i.e.\ $\beta \int_{0}^{\bar{w}} V(w') dw'$ }
  \item Argue that the optimal strategy must be to accept all wages
    above some reservation wage $w^r$. \\
    \textit{Hint: the value of rejecting a wage offer does not depend
      on the offered wage.}
  \item Given the reservation wage strategy, explain why you can write
    the value of rejecting a wage offer as
    \[ v_0 = b + \beta \left(\int_{w^r}^{\bar{w}} \frac{w'}{1-\beta}
      dw' + \frac{w^r}{\bar{w}} v_0 \right). \]
  \item The reservation wage must be such that 
    \[ v_0 = \frac{w^r}{1-\beta}. \]
    Use this equation to calculate $\frac{\partial w^r}{\partial b}$.
  \end{enumerate}
\end{problem}
\begin{solution}
  $\;$
  \begin{enumerate}
  \item If you accept a wage of $w$, you get 
    \[ \sum_{t=0}^\infty \beta^t w = \frac{w}{1-\beta}. \]
  \item Let $V(w)$ denote the value of receiving a wage offer of
    $w$. You can either accept it and get $w/(1-\beta)$, or reject and
    get $b$ plus a new offer next period. That is,
    \[ V(w) = \max\left\lbrace \frac{w}{1-\beta}, b + \beta
    \int_0^{\bar{w}} V(w') dw'/\bar{w} \right\rbrace .  \] 
    I left the division by $\bar{w}$ out of the hint, so I didn't take
    off any points for omitting it.
  \item As the hint states, the value of rejecting a wage, 
    \[ V(\text{reject})= b + \beta \int_0^{\bar{w}} V(w') dw'/\bar{w}, \]
    does not depend on the wage offered today. Therefore,
    $V(\text{reject}) = v_0$ for some constant $v_0$. Hence, it must be
    that the wage is accepted whenever $\frac{w}{1-\beta} \geq
    v_0$. In other word, you accept the wage if $w \geq (1-\beta) v_0
    = w^r$.
  \item Given the reservation wage, $V(w)$ can be written as
    \[ V(w) = \begin{cases} \frac{w}{1-\beta} & \text{ if } w \geq w^r
      \\
      v_0 & \text{ if } w < w^r
    \end{cases} \]
    and 
    \[ v_0 = b + \beta \int_0^{\bar{w}} V(w')dw'/\bar{w}. \]
    Plugging in the previous formula for $V(w')$, we have
    \begin{align*}
      v_0 = & b + \beta \left(\int_0^{w^r} v_0 dw'/\bar{w} +
      \int_{w^r}^{\bar{w}} \frac{w'}{1-\beta} dw'/\bar{w}\right) \\
      = & b + \beta \left(v_0 \frac{w^r}{\bar{w}} + \frac{\bar{w}^2 -
        (w^r)^2} {2(1-\beta) \bar{w}} \right) 
    \end{align*}
  \item 
    From the previous part, 
    \begin{align*}
      v_0 = & \frac{ b + \beta \frac{\bar{w}^2 -
          (w^r)^2} {2(1-\beta) \bar{w}}} { 1 - \beta
        \frac{w^r}{\bar{w}} } \\
      = & \frac{ b\bar{w} + \beta \frac{\bar{w}^2 -
          (w^r)^2} {2(1-\beta)}} { \bar{w} - \beta
        w^r } \\ 
    \end{align*}  
    Thus, we have,
    \begin{align*}
      \frac{w^r}{1-\beta} = & v_0 \\
      = & \frac{ b\bar{w} + \beta \frac{\bar{w}^2 -
          (w^r)^2} {2(1-\beta)}} { \bar{w} - \beta
        w^r } \\
      w^r = & \frac{ b\bar{w}(1-\beta) + \beta \frac{\bar{w}^2 -
          (w^r)^2} {2}} { \bar{w} - \beta
        w^r } 
    \end{align*}      
    You could use the quadratic formula to explicit solve for $w^r$,
    or you can just use the implicit function theorem. 
    To use the implicit function theorem, write the previous equation as
    \begin{align*}
      w^r (\bar{w}-\beta w^r) + \frac{\beta}{2} \left((w^r)^2 -
        \bar{w}^2\right) - b \bar{w}(1-\beta) = & 0 \\
      F(w^r, b) = & 0
    \end{align*}      
    Then, 
    \begin{align*}
      \frac{\partial w^r} {\partial b} (b) = & - \left( \frac{\partial
          F}{\partial w^r} \right)^{-1} \frac{\partial F}{\partial b} \\
      = & - \left(\bar{w} - 2\beta w^r + \beta w^r \right)^{-1}
      \left(-\bar{w}(1-\beta)\right) \\
      = & \frac{\bar{w}(1-\beta)} 
      {\bar{w} - \beta w^r }
    \end{align*}
    Note that this is positive because $\bar{w} > w^r$ and $\beta <
    1$. Also, 
    \[ \bar{w}(1-\beta) < \bar{w} - \beta w^r, \]
    so $\frac{\partial w^r} {\partial b} < 1$. An increase in
    unemployment benefits increases the reservation wage, but by less
    than proportionally. 
  \end{enumerate}
\end{solution}

\begin{problem}
  This is from the 2012 final. I don't especially like this problem
  anymore. 
  
  A driver is $S$ spaces from his destination. He sequentially passes
  spaces and must choose whether to park or not. Her utility from
  parking $s \in \{0,1,...,S\}$ spaces away is $-s$. If he passes all
  the spaces without parking he incurs receives utility $-D$ and must
  go back to 
  space $S$ and search again. Each time he passes a space it is
  available with independent probability $p$.
  \begin{enumerate}
  \item Write down the Bellman equation for the driver's parking
    problem. Note that the value function depends on the current
    space, $s$, but not on how long the driver has been searching. 
  \item Guess that the optimal policy will be to take the first free
    space $s^*$ or closer. Solve for the value function when following
    this policy, call this value function $v_{s^*}(s)$. \textit{Hint: the
      expression for the value function is messy.}
  \item Write down a condition such that it is optimal to park in
    space $s$ when the value function is $v_{s^*}(s)$. Verify that
    this condition holds for all $s \leq s^*$ when $s^*$ is chosen
    appropriately. \textit{Hint: in the previous part, you should
      have gotten 
      \[  v_{s^*}(s) = (1-p)^s\left(-D + v_{s^*}(s^*) \right) +
      \sum_{t=0}^s -p t(1-p)^{s-t} \]
      and you could have also solved for $v_{s^*}(s^*)$, but all that
      matters about it is that its negative. Use induction. Assume $-s^*
      > v_{s^*}(s^*)$, then show that if $-s
      > v_{s^*}(s)$, then so is $-(s-1) > v_{s^*}(s-1)$.}
  \item Explain how you could solve for $s^*$ (you need not write an
    explicit solution). 
  \end{enumerate}
\end{problem}
\begin{solution}$\;$  
  \begin{enumerate}
  \item Space $s$ is available with probability $p$. When it is
    available, you can choose to park there and get $-s$ or move on
    the next space and get $v(s-1)$. With probability $(1-p)$ the
    space is taken and you must move on and get $v(s-1)$. Thus, the
    value of being in space $s$ is
    \[ v(s) = \begin{cases} p \max\{-s, v(s-1)\} + (1-p) v(s-1) & \text{
        if } s>0\\
      (1-p)(-D + v(S) & \text{ if } s=0 \end{cases} 
    \]
    where I have already used the observation that the person would
    always park in space $0$ if its available. 
  \item If the driver always takes a space $s^*$ or closer, the value
    function is
    \[ v_{s^*}(s) = \begin{cases} v_{s^*}(s^*) & \text{ if } s>s^* \\
      -p s + (1-p) v_{s^*}(s-1) & \text{ if } 0 < s \leq s^* \\
      (1-p)(-D + v_{s^*}(S)) & \text{ if } s=0 
    \end{cases}
    \]
    We can simplify and express everything in terms of
    $v_{s^*}(s^*)$. Using the fact that $v_{s^*}(S) = v_{s^*}(s^*)$,
    for $s=0$ we get 
    \[ v_{s^*}(0) = (1-p)(-D + v_{s^*}(s^*). \]
    For $s=1$, 
    \[ v_{s^*}(1) = -p + (1-p)(1-p)(-D + v_{s^*}(s^*). \]
    For $s=2$, 
    \[ v_{s^*}(2) = -2p + (1-p)\left(-p + (1-p)(1-p)(-D +
      v_{s^*}(s^*)\right). \]
    In general, for $s\leq s^*$,
    \[ v_{s^*}(s) = (1-p)^s\left(-D + v_{s^*}(s^*) \right) +
    \sum_{t=0}^s -p t(1-p)^{s-t}. \]
    Solving for $v_{s^*}(s^*)$, 
    \begin{align*}
      v_{s^*}(s^*) = & (1-p)^s\left(-D + v_{s^*}(s^*) \right) +
      \sum_{t=0}^s -p t(1-p)^{s-t} \\
      v_{s^*}(s^*) = & \frac{-D(1-p)^{s^*} - \sum_{t=0}^{s^*} p
        t(1-p)^{s^*-t}}{1 - (1-p)^{s^*}}
    \end{align*}
  \item The driver will park in space $s$ if $-s \geq
    v_{s^*}(s-1)$. Suppose $(s+1)$ is such that $-(s+1) \geq
    v_{s^*}(s)$. Notice that
    \begin{align*}
      v_{s^*}(s-1) - v_{s^*}(s) = & (-D + v_{s^*}(s^*))
      \left((1-p)^{s-1} - (1-p)^s \right)  - p s \\
      = & (-D + v_{s^*}(s^*)) (1-p)^{s-1} - p s \leq 0 
    \end{align*}
    so 
    \[ v_{s^*}(s-1) \leq v_{s^*}(s) \leq -(s+1) < -s. \]
    By induction, we can conclude that if $s^*$ is chosen such that
    $v_{s^*}(s^*-1)< -s^*$, then for all $s<s^*$, $v_{s^*}(s-1) < -s$.
  \item $s^*$ should be the largest integer such that $v_{s^*}(s^*-1)
    < -s^*$. Such an $s^*$ exists because for $s = 0$, $v_{0}(S) \leq
    -D < 0$. Moreover, $s^*$ is bounded above above by $S$, so it must
    exist. Finally, we should verify that when $s > s^*$, it is better
    to wait than to park in an open space. By construction, $s^*$ is
    set such that $v(s^*) > -(s^*+1)$. Suppose $v(s-1) > -s$. We want
    to show that then $v(s) > -(s+1)$. Given the strategy above, $v(s)
    = v(s^*)$ for all $s\geq s^*$. Thus,
    \[ v(s) = v(s^*) = v(s-1) > -s > -(s+1) \]
    as desired. 
  \end{enumerate}
\end{solution}

\begin{problem}[\label{growth}]
  This is from the 2013 final.
  
  Consider an optimal growth model where production requires both
  labor and leisure. Time is discrete. The per-period utility function
  is $\log (c_t) + \eta \log(1-\ell_t)$ where $c_t$ is consumption and
  $\ell_t$ is labor. The discount factor is $\beta$. The production
  function is Cobb-Douglas and capital does not depreciate. The social
  planner's problem is 
  \begin{align*}
    \max_{c_t, \ell_t, k_t} & \sum_{t=0}^\infty \beta^t \left( \log
      (c_t) + \eta \log (1-\ell_t) \right) \\
    \text{ s.t. } & k_{t+1} + c_t = A k_t^\alpha \ell_t^{1-\alpha}.
  \end{align*}
  \begin{enumerate}
  \item Write the Bellman equation for this problem. 
  \item Solve for the value function. You can use any method you want,
    but I suggest guessing that $V(k) = v_0 + v_1 \log k$. 
  \item Find the optimal $\ell_t$, $c_t$, and $k_{t+1}$ as functions
    of $k_t$.
  \item Suppose total factor productivity, $A$, increases. How does
    this affect consumption, labor, and capital?
  \end{enumerate}
\end{problem}
\begin{solution} $\;$
  \begin{enumerate}
  \item The Bellman equation is
    \begin{align*}
      V(k_t) = & \max_{c,\ell,k_{t+1}} \log (c) + \eta \log(1-\ell) +
      \beta V(k_{t+1}) \\
      \text{ s.t. } k_{t+1} + c = A k_t^\alpha \ell^{1-\alpha}, 
    \end{align*}
    or substituting in the constraint,
    \begin{align*}
      V(k) = & \max_{c,\ell} \log (c) + \eta \log(1-\ell) +
      \beta V(Ak^\alpha \ell^{1-\alpha} - c).
    \end{align*}
  \item Let's guess and verify that $V(k) = v_0 + v_1 \log k$. In that
    case, the Bellman equation can be written as 
    \begin{align*}
      v_0 + v_1 \log k = & \max_{c,\ell} \log (c) + \eta \log(1-\ell) +
      \beta \left(v_0 + v_1 \log(Ak^\alpha \ell^{1-\alpha} - c) \right).
    \end{align*}
    The first order conditions are
    \begin{align*}
      [c]: && \frac{1}{c} - \frac{\beta v_1}{Ak^\alpha \ell^{1-\alpha}
        - c} = & 0 \\
      && c = & \frac{Ak^\alpha \ell^{1-\alpha}}{1+\beta v_1} \\
      [\ell]: && -\frac{\eta}{1-\ell} + \frac{\beta v_1 Ak^\alpha
        (1-\alpha)
        \ell^{-\alpha}}{Ak^\alpha \ell^{1-\alpha} - c} = & 0 \\
      && \frac{\beta v_1 Ak^\alpha (1-\alpha)
        \ell^{-\alpha}}{Ak^\alpha \ell^{1-\alpha}\left(1 -
          \frac{1}{1+\beta v_1}\right)} = & \frac{\eta}{1-\ell} \\
      && \ell = & \frac{(1+\beta v_1)(1-\alpha)}{(1 + \beta v_1)(1-\alpha) + \eta} 
    \end{align*}
    Plugging this $\ell$ and $c$ back into the Bellman equation and
    solving for $v_1$ and $v_0$:
    \begin{align*}
      v_0 + v_1 \log k = & \log\left(\frac{Ak^\alpha
          \left[\frac{(1+\beta v_1)(1-\alpha)}{(1 + \beta v_1)(1-\alpha) + \eta}
          \right]^{1-\alpha}}{1+\beta v_1}\right) + \eta 
      \log\left(\frac{\eta}{1+\beta v_1 + \eta} \right) + \\
      & + 
      \beta\left(v_0 + v_1 \log\left(A k^\alpha \left[\frac{(1+\beta
              v_1)(1-\alpha)} {(1 + \beta v_1)(1-\alpha) + \eta}
          \right]^{1-\alpha} \left(1 - 
            \frac{1}{1+\beta v_1 } \right) \right) \right)
    \end{align*}
    Taking the derivative of each side with respect to $\log k$, we
    have
    \begin{align*}
      v_1 = & \alpha + \beta v_1 \alpha \\
      v_1 = & \frac{\alpha}{1-\beta \alpha}.
    \end{align*}
    We could plug this back in to solve for $v_0$, but it is quite
    messy and not actually needed to find $c$ and $\ell$. 
  \item Combining expressions from the previous part, 
    \begin{align*}
      \ell_t^\ast(k) = & \frac{1-\alpha}{1-\alpha + \eta(1-\beta\alpha)} 
    \end{align*},
    \begin{align*}
      c_t^\ast(k) = & Ak^\alpha\left(\frac{1-\alpha}{1-\alpha +
          \eta(1-\beta\alpha)} \right)^{1-\alpha} (1-\beta \alpha),
    \end{align*}
    and
    \begin{align*}
      k_{t+1}^\ast(k) = & Ak^\alpha\left(\frac{1-\alpha}{1-\alpha +
          \eta(1-\beta\alpha)} \right)^{1-\alpha} \beta \alpha.
    \end{align*}
  \item Labor does not depend on productivity, so it is
    unchanged. Output increases proportionally with $A$. A portion of
    the increased output, $(1-\beta \alpha)$ of it, is consumed. The
    remaining $\beta\alpha$ of the increase in output is used to
    increase $k_{t+1}$. 
  \end{enumerate}    
\end{solution}

\begin{problem}
  This is from the 2014 midterm. 
  
  Consider a driver with a bonus-malus insurance coverage (somewhat
  like ICBC). The driver is initial placed in some risk category $s
  \in \{0, ..., S \}$. Time is discrete. If the driver makes a claim
  in period $t$, then the driver is moved up ono category to
  $\min\{s+1, S\}$. If the driver does not make a claim, then she is
  moved down to $\max\{s-1,0\}$. Each period the driver must pay some
  premium, $p(s)$, for insurance, with $p$ increasing with
  $s$. Accidents occur with probability $\pi$. The cost of an
  accident, $c \in [0, \infty)$, is drawn from a distribution with pdf
  $f$ and cdf $F$. When there is an accident, the driver can choose to
  pay the cost $c$ and not make an insurance claim, or to make an
  insurance claim and pay some deductible $d(s)$. The driver's
  discount factor is $\beta$. We want to figure out which accidents the
  driver will claim.
  \begin{enumerate}
  \item What are the state variables and controls in this model?
  \item Write the Bellman equation for this model.
  \item Argue that the optimal decision is to claim an accident when
    in category $s$ if it costs more than some threshold, $c(s)$.
  \item Express $c(s)$ in terms of the value function and the
    parameters of the model.
  \item\label{ins:para} Assume $S = 1$ so that there are only two risk
    categories, and assume that $c$ is uniformly distributed on
    $(0,1)$. Also assume that $d(0) = d(1)$. Solve for $c(s)$ and the
    value function. [Hint: first show that $c(0) = c(1)$. Then find
    the difference in the expected values between $s=1$ and $s=0$.]
  \item (Challenging --- only attempt if you have extra time) Suppose
    that driver can exert effort to change the probability of an
    accident. Let $g(\pi)$ be cost of the effort needed to make $\pi$
    be the probability of an accident. Set up the dynamic programming
    problem. Solve the problem under the assumptions of part
    \ref{ins:para} and with $g(\pi) = (a - \pi)^2$ for $a>0$.  
  \end{enumerate}
\end{problem}
\begin{solution}
  \begin{enumerate}
  \item The state variables are the risk category $s$ and the cost of
    the accident $c$. The control is whether to make an insurance
    claim.
  \item The Bellman equation is
    \begin{align*}
      V(s,c) = \max \left\lbrace -c - p(s) + \beta \Er[V(s-1,c')] , -d(s) -
        p(s) + \beta \Er[V(s+1,c')] \right\rbrace
    \end{align*}
  \item The value of not claiming, $  -c - p(s) + \beta \Er[V(s-1,c')]$
    decreases with $c$. The value of claiming, $ -d(s) -
    p(s) + \beta \Er[V(s+1,c')] $ does not depend on $c$. Therefore,
    the optimal strategy must be to not claim whenever 
    \[ c \leq \beta \Er[V(s-1,c')] - \beta \Er[V(s+1,c')] + d(s) \]
  \item As just stated in the previous part, $c(s) = \beta
    \Er[V(s-1,c')] - \beta \Er[V(s+1,c')] + d(s)$ 
  \item With only $S=2$ categories, the cutoffs are
    \begin{align*}
      c(0) = & \beta
      \Er[V(0,c')] - \beta \Er[V(1,c')] + d(0) \\
      c(1) = & \beta
      \Er[V(0,c')] - \beta \Er[V(1,c')] + d(1).
    \end{align*}
    These can only differ through $d$. Therefore, when $d(0) = d(1)$,
    $c(0) = c(1)$ as well. To solve for $c(0)$, notice that
    \begin{align*}
      V(0,c) - V(1,c) = & \max \left\lbrace -c - p(0) + \beta \Er[V(0,c')] , -d -
        p(0) + \beta \Er[V(1,c')] \right\rbrace - \\
      & - \max \left\lbrace -c - p(1) + \beta \Er[V(0,c')] , -d -
        p(1) + \beta \Er[V(1,c')] \right\rbrace \\
      = & -p(0) + p(1)
    \end{align*}
    so,
    \begin{align*}
      c^* = c(0)=c(1) = & \beta\Er[V(0,c') - V(1,c')] + d \\
      = & \beta \left( p(1) - p(0) \right) + d
    \end{align*}
    Finally, to find the value function, note that
    \begin{align*}
      \Er[V(s,c)] = & -p(s) + (1-\pi)(\beta \Er[V(0,c')] + \pi
      \int_0^{c^*} -c + \beta \Er[V(0,c')] dc + 
      \pi \int_{c^*}^1 -d + \beta \Er[V(1,c')] dc \\
      \Er[V(s,c)] = & -p(s) + (1-\pi + \pi c^* ) \beta \Er[V(0,c')] - \pi
      {c^*}^2/2  - \pi (1-c^*) d + \pi (1-c^*) \beta \Er[V(1,c')]
    \end{align*}
    so
    \begin{align*}
      \Er[V(0,c)] = \frac{-p(0) - \pi
        {c^*}^2/2  - \pi (1-c^*) d + \pi (1-c^*) \beta
        \Er[V(1,c)]}{1 - \beta(1-\pi + \pi c^*)}
    \end{align*}
    and 
    \begin{align*}
      \Er[V(1,c)] = & -p(1) - \frac{\beta (1-\pi + \pi c^*) p(0)}{1 -
        \beta(1-\pi + \pi c^*)} + \\
      & + \left(1 + \frac{\beta (1-\pi + \pi
          c^*) }{1 - \beta(1-\pi + \pi c^*)} \right) \left(-\pi
        {c^*}^2/2  - \pi (1-c^*) d + \pi(1-c^*) \beta
        \Er[V(1,c)]\right) \\
      = & \frac{-p(1) + \beta (1-\pi + \pi c^*) (p(1) - p(0)) + -\pi
        {c^*}^2/2  - \pi (1-c^*) d}{1 - \beta}
    \end{align*} 
  \end{enumerate}
\end{solution}


% \bibliographystyle{jpe}
%\bibliography{../../526}

\end{document}
