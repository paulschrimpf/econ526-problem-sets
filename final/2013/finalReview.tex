\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage[normalem]{ulem}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\newtheorem{problem}{Problem}
\theoremstyle{definition}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}
\newtheorem{solution}{Solution}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 
\def\F{\mathbb{F}}
\def\R{\mathbb{R}}
\newcommand{\gmatrix}[1]{\begin{pmatrix} {#1}_{11} & \cdots &
    {#1}_{1n} \\ \vdots & \ddots & \vdots \\ {#1}_{m1} & \cdots &
    {#1}_{mn} \end{pmatrix}}
\newcommand{\iprod}[2]{\left\langle {#1} , {#2} \right\rangle}
\renewcommand{\det}{\mathrm{det}}
\newcommand{\rank}{\mathrm{rank}}
\newcommand{\norm}[1]{ {\left\Vert {#1} \right\Vert} }

\title{Review questions}
\date{\today}

\begin{document}
\renewcommand{\theenumi}{\roman{enumi}}

\maketitle

\section{Outline}

\begin{enumerate}
\item Functions
  \begin{itemize}
  \item Continuity
  \end{itemize}
\item Differential calculus
  \begin{enumerate}
  \item Partial derivatives
  \item Total derivative
  \item Mean value theorem
  \item Chain rule
  \item Higher order derivatives
  \item Taylor expansions
  \end{enumerate}
\item Implicit and inverse function theorems
  \begin{enumerate}
  \item Inverse functions
  \item Implicit functions 
  \item Contraction mappings
    \begin{itemize}
    \item Fixed point theorem
    \end{itemize}
  \item Roy's identity and comparative statics examples 
  \end{enumerate}
\item Unconstrained optimization
  \begin{enumerate}
  \item maximizer, strict, local
  \item first order conditions
  \item second order condition
    \begin{itemize}
    \item negative and positive (semi-) definite
      \begin{itemize}
      \item know how to check 
      \item leading principal minors
      \item eigenvalues and eigenvectors
      \end{itemize}
    \item Hessian
    \end{itemize}
  \end{enumerate}
\item Constrained optimization
  \begin{enumerate}
  \item First order conditions
  \item Lagrangian
  \item Second order condition
  \item Multiplier interpretation
  \item Envelope theorem
  \item Applications
    \begin{itemize}
    \item Extremum estimators: first order condition, mean value
      expansion 
    \item Pollution example: find social planner solution, find
      competitive equilibrium, find taxes that implement social
      planner solution
    \end{itemize}
  \end{enumerate}
\item Optimal control and dynamic programming
  \begin{enumerate}
  \item Differentiation in vector spaces
  \item Optimization in vector spaces: first order condition
  \item Optimal control
    \begin{itemize}
    \item Continuous time: be able to write down first order
      conditions, either through optimization in vector spaces, or by
      using the Hamiltonian
    \item Discrete time
    \end{itemize}
  \item Dynamic programming
    \begin{enumerate}
    \item Value function and Bellman equation
    \item Finite and infinite horizon
    \item Bellman equation as a contraction mapping
    \end{enumerate}
  \end{enumerate}
\end{enumerate}

\section{Questions}

\begin{problem}[SB 14.28]
  Let $f:\R^2 \to \R$ be 
  \[ f(x,y) = \begin{cases} 0 & \text{ if } (x,y) = (0,0) \\
    \frac{x^3 y - x y^3}{x^2+y^2} & \text{ otherwise} 
  \end{cases}
  \]
  \begin{enumerate}
  \item Show that $f(x,0) = f(0,y) = 0 $ for all $x$ and $y$. Conclude
    that $\frac{\partial f}{\partial x}(0,0) = \frac{\partial
      f}{\partial y}(0,0) = 0$.
  \item Compute $\frac{\partial f}{\partial x}$  and $\frac{\partial
      f}{\partial y}$ for $(x,y) \neq (0,0)$.
  \item Conclude that $\frac{\partial f}{\partial x}(0,y) = -y$  and
    $\frac{\partial f}{\partial y}(x,0) =  x$.
  \item\label{sbd} Show that 
    \[ \frac{\partial^2 f}{\partial y \partial x}(0,0) =
    \frac{\partial}{\partial y}\left(\frac{\partial f}{\partial
        x}\right)(0,0) = -1. \]
  \item\label{sbe} Show that
    \[ \frac{\partial^2 f}{\partial x \partial y}(0,0) =
    \frac{\partial}{\partial x}\left(\frac{\partial f}{\partial
        y}\right)(0,0) = 1. \]
    Notice that \ref{sbd} and \ref{sbe} appear to contradict theorem
    1.7 from lecture 8 (theorem 14.5 in Simon and Blume).
  \item\label{sbf} Compute $\frac{\partial^2 f}{\partial x \partial y}(x,y)$ for
    $(x,y) \neq 0$. 
  \item\label{sbg} Use \ref{sbf} to show that $\frac{\partial ^2 f}{\partial
      x \partial y}(x,x) = 0$ for $x>0$.
  \item Compare \ref{sbe} and \ref{sbg} to show that $\frac{\partial^2
      f}{\partial x \partial y}(x,y)$ is not continuous at
    $(0,0)$. Reconcile \ref{sbd} and \ref{sbe} with theorem 1.7 from
    lecture 8.
  \end{enumerate}
\end{problem}
\begin{solution}
  \begin{enumerate}
  \item $f(x,0) = \frac{0}{x^2}$ and $f(0,y) = \frac{0}{y^2}$, so both
    are zero, and the partial derivative at 0 are also 0.
  \item 
    \begin{align*}
      \frac{\partial f}{\partial x} = \frac{3x^2 y - y^3}{x^2+y^2} -
      \frac{x^3 y - xy^3}{(x^2+y^2)^2} 2x 
    \end{align*}
    \begin{align*}
      \frac{\partial f}{\partial y} = \frac{x^3  - 3y^2 x}{x^2+y^2} -
      \frac{x^3 y - xy^3}{(x^2+y^2)^2} 2y 
    \end{align*}
  \item  \label{p1.3}
    \begin{align*}
      \frac{\partial f}{\partial x}(0,y) = \frac{- y^3}{y^2} = -y
    \end{align*}
    \begin{align*}
      \frac{\partial f}{\partial y}(x,0) = \frac{x^3}{x^2} = x
    \end{align*}
  \item From the previous part, $\frac{\partial f}{\partial x}(0,y) =
    -y$, so $\frac{\partial}{\partial y} \frac{\partial f}{\partial
      x}(0,0) = -1$.
  \item\label{p1e} From part \ref{p1.3}, $\frac{\partial f}{\partial y}(x,0) =
    x$, so $\frac{\partial}{\partial x} \frac{\partial f}{\partial
      y}(0,0) = 1$.
  \item\label{p1f}     
    \begin{align*}
      \frac{\partial^2 f}{\partial y \partial x} = \frac{3x^2-
        3y^2}{x^2+y^2} - \frac{3x^2 y - y^3}{(x^2+y^2)^2} 2y -
      \frac{x^3 - 3xy^2}{(x^2+y^2)^2} 2x +  \frac{x^3y -
        xy^3}{(x^2+y^2)^3} 8x (x^2+y^2) y 
    \end{align*}
  \item\label{p1g} From the previous part, 
    \begin{align*}
      \frac{\partial^2 f}{\partial y \partial x}(x,x) = & 0 - \frac{2x^3}{4x^4} 2x -
      \frac{-2x^3}{4x^4} 2x +  0  \\
      = & 0
    \end{align*}
  \item From \ref{p1e}, for any $x>0$, $y=0$, $\frac{\partial^2
      f}{\partial y \partial x}(x,0) = -1$. From \ref{p1g} for any
    $x>0$, $y=x$, we have
    $\frac{\partial^2 f}{\partial y \partial x}(x,x) = 0$. In any
    neighborhood of $0$, there is some point with $x>0$ and $y=0$, and
    another with $x>0$, $y=x$. Therefore, the second derivative is not
    continuous at $0$. Theorem 1.7 from lecture 8 says that the order
    of partial derivatives does not matter when the function is
    continuously twice differentiable. The second derivative of $f$ is
    not continuous, so that theorem need not hold here.
  \end{enumerate}
\end{solution}

\begin{problem}
  For what values of their parameters do the following production
  functions, $q = f (k, l)$, obey the law of diminishing marginal
  returns, that is $\frac{\partial^2  f}{\partial k^2} < 0$ and
  $\frac{\partial^2  f}{\partial l^2} < 0$ ?
  \begin{enumerate}
  \item for the Cobb-Douglas: $q = f (k, l) = k^\alpha l^\beta$
  \item for the CES: $q = f (k, l) = [\alpha k^\rho + \beta l^\rho
    ]^{1/\rho}$
  \end{enumerate}  
\end{problem}
\begin{solution}
  \begin{enumerate}
  \item Calculate:
    \begin{align*}
      \frac{\partial f}{\partial k} = & \alpha k^{\alpha-1}l^\beta \\
      \frac{\partial^2 f}{\partial k^2} = & \alpha(\alpha-1)
      k^{\alpha-2}l^\beta \\
    \end{align*}
    and
    \begin{align*}
      \frac{\partial f}{\partial l} = & \beta k^{\alpha}l^{\beta-1} \\
      \frac{\partial^2 f}{\partial l^2} = & \beta(\beta-1)
      k^{\alpha}l^{\beta-2} \\
    \end{align*}
    These second derivatives are negative if $\alpha<1$ and $\beta<1$.    
  \end{enumerate}
\end{solution}

\begin{problem}
  Compute the following:
  \[ \max_{x_1,x_2} = x_1 x_2^2 - \alpha x_1 + \gamma x_2 \]
  where $\alpha,\gamma > 0$ are parameters. Be sure to check the
  second order condition.
\end{problem}
\begin{solution}
  The first order conditions are
  \begin{align*}
    x_2^2 - \alpha = & 0 \\
    2x_1 x_2 + \gamma = & 0
  \end{align*}
  The critical points are $x_2 = \pm \sqrt{\alpha}$ and $x_1 =
  \mp \frac{\gamma}{2{\sqrt{\alpha}}}$. The matrix of second
  derivatives is
  \begin{align*}
    D^2 f_x = & 
    \begin{pmatrix} 0 & 2 x_2 \\
      2 x_2 & 2 x_1 
    \end{pmatrix}.
  \end{align*}
  Its determinant is $-4x_2^2 < 0$. Its first principle minor is
  $0$. Therefore, the Hessian is not negative semidefinite (we would
  need the principle minor to be $\leq 0$ and determinant  $\geq
  0$). The critical points above are not maximizers. There is no
  maximizer because, for example, with $x_1 = 0$, the function is
  always increasing in $x_2$.
\end{solution}

\begin{problem}
  Let $f:\R^n \to \R$ be twice continuously differentiable. Suppose
  the maximizer of $f$ on $\R^n$ is $x^*$. Let $g:\R \to \R$ be twice
  continuously differentiable with $g'(x) > 0$ for all $x \in
  \R$. Prove that 
  \[ \max_{x \in \R^n} g(f(x)) = g(f(x^*)). \]
\end{problem}
\begin{solution}
  One approach to this problem is to try to show that the first and
  second order conditions for $f$ imply them for $g(f(x))$. We will
  try this, but it's not quite going to work.
  By assumption, $x^∗$ is a maximizer of $f$, and $f$ is twice
  continuously differentiable. Therefore, the first and second order
  conditions hold for $f$ at $x^*$. That is,
  \[ Df_{x^*} = 0 \]
  and $D^2 f_{x^*}$ is negative semi-definite. Therefore, 
  \[ g'(f (x^*)) D_f(x^*) = 0 \]
  i.e.\ $x^*$ satisfies the first order condition for maximizing
  $f(g(x))$. Furthermore,
  \[ g'' (f (x^∗ )) Df_{x^∗}  + g'(f (x^∗ )) D^2 f_{x^∗} = g'(f (x^∗
  )) D^2 f_{x^∗} \leq 0. \]
  If we knew that $D^2 f_{x^*}$ were negative definite instead of
  negative semidefinite, we would be done. However, we do not know
  this, so we should take a different approach. 

  Suppose $x_0$ is a maximizer of $g(f(x))$. By the mean value
  theorem,
  \[ g(f(x^*)) - g(f(x_0)) = g'(\bar{y}) ( f(x^*) - f(x_0)) \]
  for some $\bar{y}$ between $f(x^*)$ and $f(x_0)$. By definition of
  $x_0$, 
  \[ g(f(x^*)) - g(f(x_0)) \leq 0. \]
  Also, since $g'>0$ and $f(x^*) \geq f(x_0)$,
  \[ g(f(x^*)) - g(f(x_0)) \geq 0. \]
  Thus, $g(f(x^*)) - g(f(x_0)) = 0$, and since $g'>0$, it must be that
  $f(x^*) = f(x_0)$. Thus, $x^*$ is also a maximizer of $g(f(x))$.  
\end{solution}

\begin{problem}
  Maximize $f (x, y) = -(x - 3y + 1)^2 - y^2 + 4$ subject to the
  constraint $x + y = 4$. 
\end{problem}
\begin{solution}
  The first order conditions are
  \begin{align*}
    -2(x-3y+1) - \lambda = & 0 \\
    6(x-3y +1) - 2y -\lambda = & 0 \\
    x + y = & 4 
  \end{align*}
  The solution to this system of equations is $x^* = 48/17$, $y^* =
  20/17$ and $\lambda^* = 10/17$. We should also check the second
  order condition. You could this by looking at the bordered Hessian,
  but maybe you don't remember what that condition looks like. A
  perhaps  easier way is to substitute in the constraint $y= 4-x$, and
  then look at Hessian of the new unconstrained problem. After
  substituting in the constraint, the objective function will be
  something like $-Ax^2 + Bx + C$, where $A>0$ (it's not hard to
  figure out exactly what $A$, $B$, and $C$ are). The Hessian is just
  $-A$, so the second order condition holds. 
\end{solution}

\begin{problem}
  In a Stackelberg game, there are two firms competing in
  quantities. The difference being one firm is allowed to produce
  first and therefore, has a first-mover advantage. Assume that the
  inverse market demand is given by the linear equation:
  \[ P = a - bQ \]
  where $Q = q_1 + q_2$ where the index $i = 1, 2$ denotes the two
  firms. Furthermore, assume that costs for both firms are identical
  with marginal costs being constant and equal to $c$. 
  \begin{enumerate}
  \item Firm $2$ chooses $q_2$ to maximize profits treating $q_1$ as
    given. Find the second firm’s optimal output as a function of the
    parameters of the model. 
  \item Firm $1$ chooses $q_1$ to maximize profits, taking into
    account that $q_2$ will depend on $q_1$. State the
    first firm's optimization problem and find the solution.
  \end{enumerate}
\end{problem}
\begin{solution}
  \begin{enumerate}
  \item Firm 2's problem is
    \[ \max_{q_2} q_2 (a - b(q_1 + q_2) - c)  \]
    The first order condition is
    \[ a - bq_1 - c -b2q_2 = 0 \]
    so 
    \[ q_2 = \frac{a - c - b q_1}{2 b}. \] 
    And the second order condition also holds.
  \item Firm 1's problem is
    \[ \max_{q_1} q_1 \left( a - b (q_1 +  \frac{a - c - b q_1}{2 b})
      - c\right) \]
    The first order condition is
    \[ a - b q_1 - \frac{a-c}{2} - c = 0 \]
    so
    \[ q_1 = \frac{a-c}{2b}. \]    
  \end{enumerate}
\end{solution}

\begin{problem}
  Maximize household utility given by 
  \[ u (x_1 , x_2 ) = (x_1^2 + x_2^2)^{1/2} \]
  given prices $p_1 > 0$ and $p_2 > 0$ and income level of
  $M >
  0$. $x_1$ and $x_2$ are constrained to be non-negative.
\end{problem}
\begin{solution}
  
\end{solution}

\begin{problem}
  A function $f:\R^n \to \R$ is strictly concave if 
  \[ f(x\lambda + (1-\lambda)y ) > \lambda f(x) + (1-\lambda)f(y) \]
  for all $x,y \in \R^n$ with $x\neq 0$ and $\lambda \in
  (0,1)$. Suppose $f$ is twice continuously 
  differentiable. Prove that $f$ is strictly concave if and only if
  its Hessian is negative definite. 
  \textit{Hint: consider $g(\lambda)
  = f(x\lambda + (1-\lambda)y )$. Apply the mean value theorem to
  $\lambda[g(\lambda) - g(1)]$ and $(1-\lambda)[g(\lambda) - g(0)]$.}
\end{problem}
\begin{solution}
  Define $g(\lambda)$ as in the hint. Notice that
  \[ \lambda f(x) + (1-\lambda) f(y) = \lambda g(1) + (1-\lambda) g(0). \]
  Consider
  \begin{align*}
    g(\lambda) - \left( \lambda g(1) + (1-\lambda) g(0) \right) = &
    \lambda\left[ g(\lambda) - g(1) \right] + (1-\lambda)
    \left[g(\lambda) - g(0) \right].
  \end{align*}
  Since $f$ is twice continuously differentiable, so is $g$. In
  particular, we the mean value theorem applies to both $g$ and
  $g'$. By the mean value theorem for $g$,
  \begin{align*}
    g(\lambda) - g(1) = & g'(\bar{\lambda}_1) (\lambda-1) \\
    g(\lambda) - g(0) = & g'(\bar{\lambda}_0) (\lambda-0)
  \end{align*}
  for some $\bar{\lambda}_1 \in [\lambda,1]$ and $\bar{\lambda}_0 \in
  [0,\lambda]$. Thus,
  \begin{align*}
    g(\lambda) - \left( \lambda g(1) + (1-\lambda) g(0) \right) = &
    \lambda(\lambda-1) g'(\bar{\lambda}_1) + (1-\lambda) \lambda
    g'(\bar{\lambda}_0) \\
    = & \lambda(1-\lambda) \left[ g'(\bar{\lambda}_0) -
      g'(\bar{\lambda}_1) \right]
  \end{align*}
  Applying the mean value theorem to $g'$, we get
  \begin{align*}
    g(\lambda) - \left( \lambda g(1) + (1-\lambda) g(0) \right) 
    = & \lambda(1-\lambda) (\bar{\lambda_0} - \bar{\lambda}_1) g''(\bar{\lambda}_2) 
  \end{align*}
  For some $\bar{\lambda}_2 \in [\bar{\lambda}_0
  ,\bar{\lambda}_1]$. Note that $\bar{\lambda}_0 < \bar{\lambda}_1$,
  so 
  \[ g(\lambda) - \left( \lambda g(1) + (1-\lambda) g(0) \right) >
  0 \]
  if and only if $g''(\bar{\lambda}_2)<0$. That is, $f$ is concave if
  and only if $g''(\bar{\lambda}_2)<0$ for all possible $g$ ($g$
  implicitly depends on $x$ and $y$). $g'$ and $g''$ can be written in
  terms of $f$ as
  \begin{align*}
    g'(\lambda) = &  Df_{x\lambda +
      (1-\lambda)y} (x - y) \\
    g''(\lambda) = & (x - y)^T D^2 f_{x\lambda +(1-\lambda)y} (x - y) 
  \end{align*}
  Thus, $g''(\bar{\lambda}_2) < 0$ for all $g$ if and only if 
  $D^2f_{x\bar{\lambda}_2 +(1-\bar{\lambda}_2)y}$  
  is negative definite for all $x$ and $y \in \R^n$. This can be true
  if and only if $D^f_{x}$ is negative definite for all $x \in \R^n$.  
\end{solution}

\begin{problem}
  A consumer receives income $y_t$ each period. His discount rate
  is $\beta$ and his per-period utility function is $\log c_t$. The
  interest rate is $r$. The consumer's problem is
  \begin{align}
    \max_{c_t} &\sum_{t=0}^\infty \beta^t \log c_t \\
    & \text{s.t.} \notag \\
    & \sum_{t=0}^\infty \frac{y_t} {(1+r)^t} = \sum_{t=0}^\infty
    \frac{c_t} {(1+r)^t}  \label{pvbc}
  \end{align}
  \begin{enumerate}
  \item Find the maximizer $c_t^*$.
  \item Let $s_t$ be savings at time $t$. That is, set $s_0 = 0$,
    $s_1 = (1+r) (y_0-c_0)$, $s_2 = (1+r)(y_1 + s_1 - c_1)$, etc. 
    \begin{enumerate}
    \item Show that the constraints
      \begin{align}
        s_{t+1} = (1+r)(y_t + s_t - c_t) \label{bc1}
      \end{align}
      for $t=1,2, ...$  do not imply the present value budget
      constraint (\ref{pvbc}). 
    \item Show that 
      \begin{align}
        \lim_{t \to \infty} \frac{s_t}{(1+r)^t} = 0 \label{tc}
      \end{align}
      and (\ref{bc1}) are together equivalent to
      (\ref{pvbc}). (\ref{tc}) is the transversality condition for
      this problem.
    \end{enumerate}
  \end{enumerate}
\end{problem}
\begin{solution}
  \begin{enumerate}
  \item The first order conditions are
    \begin{align*}
      \beta^t c_t^{-1}- \lambda(1+r)^{-t} = & 0 \\
      \sum_{t=0}^\infty \frac{y_t} {(1+r)^t} - \sum_{t=0}^\infty
      \frac{c_t} {(1+r)^t} = & 0
    \end{align*}
    Thus, 
    \[ c_t = \lambda \beta^t (1+r)^t \]
    and we can use the budget constraint to solve for $\lambda$,
    \begin{align*}
      \sum_{t=0}^\infty \frac{y_t} {(1+r)^t} =& \sum_{t=0}^\infty
      \frac{\lambda (1+r)^t \beta^t} {(1+r)^t} \\
      \sum_{t=0}^\infty \frac{y_t} {(1+r)^t} =& \lambda
      \frac{1}{1-\beta}  \\
      Y(1-\beta) = & \lambda 
    \end{align*}
    where $Y \equiv \sum_{t=0}^\infty \frac{y_t} {(1+r)^t}$. So we
    have
    \[ c_t^* = Y (1-\beta) \beta^t (1+r)^t. \]
  \item Any sequence of $c_t$ can satisfy
    \[ s_{t+1} = (1+r)(y_t + s_t - c_t) \]
    by choosing $s_t$ appropriately. However, $c_t = c_0 (1+r)^t$ for
    some $c_0>0$ makes $\sum_{t=0}^\infty c_t(1+r)^{-t}$ diverge to
    $\infty$. 
  \item Note that \ref{pvbc} can be written as
    \[ \sum_{t=0}^\infty \frac{y_t - c_t}{(1+r)^t} = 0 \]
    which by definition means that
    \[ \lim_{T \to \infty} \sum_{t=0}^T \frac{y_t - c_t}{(1+r)^t} =
    0. \]
    Recursively applying \ref{bc1} and setting $s_0 = 0$ gives
    \[ s_t = \sum_{\tau=0}^{t-1} (1+r)^{t-\tau} (y_\tau - c_\tau). \]
    So,
    \[ \frac{s_t}{(1+r)^t} = \sum_{\tau=0}^{t-1} (1+r)^{-\tau} (y_\tau
    - c_\tau). \]
    and $\lim_{t \to \infty} \frac{s_t}{(1+r)^t}$ is the same as
    \ref{pvbc}. 
  \end{enumerate}
\end{solution}

\begin{problem}
  A consumer  receives income $y$ each period. His discount rate
  is $\beta$ and his per-period utility function is $\log c_t$. The
  interest rate is $r$. Assume that $\beta = \frac{1}{1+r}$. The
  consumer's dynamic programming problem is 
  \begin{align}
    V(s_t) = &\max_{c_t,s_{t+1}} \log c_t + \beta V(s_{t+1}) \\
    & \text{s.t. } s_{t+1} = (1+r)(s_t + y - c_t)
  \end{align}
  Guess and verify that $V(s) = \alpha_0 \log( \alpha_1 s + \alpha_2
  y)$. Solve for $\alpha_0$, $\alpha_1$, and $\alpha_2$.
\end{problem}
\begin{solution}
  If we substitute in the constraint and use the guessed form of the
  value function, the first order condition for $c$ is
  \begin{align*}
    \frac{1}{c} = & \beta \alpha_0
    \frac{\alpha_1(1+r)} {\alpha_1(1+r)(s+y-c) + \alpha_2 y} \\
    c \alpha_1(1+r)(1 + \beta\alpha_0)= & \alpha_1(1+r)(s+y) +
    \alpha_2 y \\
    c = & \frac{s+y}{1+\beta\alpha_0}  + \frac{\alpha_2 y}{\alpha_1
      (1+r) (1+\beta\alpha_0) }
  \end{align*}
  Substituting this into the Bellman equation, we have
  \begin{align*}
    \alpha_0 \log(\alpha_1 s + \alpha_2 y) = & \log
    \left(\frac{s}{1+\beta \alpha_0} + \frac{y(\alpha_2 +
        \alpha_1(1+r))}{\alpha_1(1+r)(1+\beta \alpha_0)}\right) + \\
      & + \beta \log \left( \alpha_1s\frac{\beta \alpha_0
          (1+r)}{1+\beta\alpha_0} + 
        y \frac{\beta \alpha_0(1+r) \alpha_1 - \alpha_2} 
        {\alpha_1 (1+r)(1+\beta \alpha_0)} + \alpha_2 y
      \right)
    \end{align*}
      
\end{solution}

\begin{problem}
  A worker can spend effort acquiring human capital or producing
  output. Let $x(t) \in[0,1]$ be portion of effort devoted to
  acquiring human capital at time $t$. Suppose the stock of human
  capital, $k(t)$, evolves according to 
  \begin{align}
    \frac{dk}{dt}(t) = a x(t) k(t) - \delta k(t).  \label{dk}
  \end{align}
  Suppose that total output is
  \[ \int_{0}^T c (1-x(t)) k(t) dt. \]
  Find the function $x(t)$ that maximizes total output. Assume that
  $a>\delta >0$ and $k(0) = k_0 > 0$.
\end{problem}
\begin{solution}
  The Lagrangian is 
  \begin{align*}
    L(x,k,\lambda,\mu_1,\mu_0,\psi) = & \int_{0}^T c (1-x(t)) k(t) dt -
    \int_0^T \lambda(t)\left(\frac{dk}{dt}(t) - ax(t) k(t) + \delta
      k(t) \right) dt - \\ 
    & - \int_0^T \mu_1(t)(x(t)-1) dt + \int_0^T \mu_0(t)
    x(t) dt - \psi(k(0) - k_0)
  \end{align*}
  where $\lambda$ is the multiplier for (\ref{dk}), $\mu_1$ is the
  multiplier for $x(t) \leq 1$, $\mu_0$ for $-x(t) \leq 0$, and $\psi$
  for the initial condition.

  The first order conditions are
   \begin{align*}
    [x]: 0 = D_x L(v) = & \int_0^T \left(-ck(t) + \lambda(t) a k(t) -
      \mu_1(t) + \mu_0(t)\right) v(t) dt \\
    [k]: 0 = D_k L(v) = & \int_0^T c(1-x(t))v(t) - \lambda(t)\left(
      \frac{dv}{dt}(t) - (ax(t) - \delta)v(t)\right) dt - \psi v(0) \\
    = & \int_0^T v(t) \left(c(1-x(t)) - \lambda(t) (a x(t)
      -\delta)\right)  + \int_0^T
    \frac{d\lambda}{dt}(t) v(t) \\ 
    & - \lambda(T)v(T) + \lambda(0) v(0) - \psi v(0) \\
    [\lambda]: 0 = D_\lambda L(v) = & \int_0^T
    v(t)\left(\frac{dk}{dt}(t) - ax(t) k(t) + \delta k(t) \right) dt
    \\
    [\mu_1]:  0 = \mu_1 D_{\mu_1}(v) = & \int_0^T \mu_1(t) v(t)(x(t) - 1)
    dt \\
    [\mu_0]:  0 = \mu_0 D_{\mu_0}(v) = & \int_0^T \mu_0(t)v(t) x(t)
    dt \\
    [\psi]:  k_0 = k(0) \;\;\;&  
  \end{align*}
  Each of these must hold for all functions $v$. Since $\mu_1(t)$ is
  positive only if $x(t) = 1$ and $\mu_0(t)$ is positive only if $x(t)
  = 0$, from $[x]$, we see that
  \begin{align*}
    x(t) = \begin{cases} 0 & \text{ if } \lambda(t) a - c < 0 \\
      1 & \text{ if } \lambda(t) a  - c > 0 
    \end{cases}
  \end{align*}
  Because of the $\lambda(T)v(T)$ term in $[k]$, we know that
  $\lambda(T) = 0$. Therefore, $x(T) = 0$. Also, from $[k]$,
  \begin{align}
    \frac{d\lambda}{dt} = & -\lambda(t)\left( a x(t) - \delta\right) -
    c(1-x(t))  \label{dl}
  \end{align}
  It follows that for $t$ slightly less than $T$, we must still have
  $x(t) = 0$, and $\frac{d\lambda}{dt}<0$. As $t$ decreases,
  $\lambda(t)$ increases. At some point we will have $\lambda(t^*) a =
  c$. At this instant, $x(t^*)$  has not been determined, but 
  \[ \frac{d\lambda}{dt}(t^*) = -c x(t^*) + \delta c /a - c(1-x(t^*))
  = c \left(\frac{\delta}{a}-1 \right) < 0 \]
  so regardless of $x(t^*)$, $\lambda(t)$ is greater than
  $\frac{c}{a}$ for $t < t^*$, so $x(t) = 1$ for $t<t^*$. 

  All that remains is to find $t^*$. From \ref{dl},
  \begin{align*}
    \frac{d \lambda}{dt} = & -\lambda(t)a 1\{t<t^*\} -
    \lambda(t)\delta  - c 1\{t>t^* \}
  \end{align*}
  Guess that $\lambda(t) = A_0 e^{A_1 t} + A_2$. Then $\frac{d
    \lambda}{dt} = A_0 A_1 e^{A_1 t}$. The above equation holds for
  $t>t^*$ if $A_0 A_1 = A_0 \delta$ and $A_2 \delta =
  c$. Additionally, we must have $\lambda(T) = 0$, so 
  \[ A_0 e^{A_1 T } = -\frac{c}{\delta} \].
  Finally, since $\lambda'(t)<0$, we should make $A_0 < 0$. One
  solution is $A_0 = -\frac{c}{\delta} e^{\delta T}$, $A_1 =
  -\delta$. Then,
  \[ \lambda(t) = \frac{c}{\delta} \left(1- e^{-\delta(T-t)} \right). \]
  Now to solve for $t^*$,
  \begin{align*}
    \frac{c}{a} = & \lambda(t*) = \frac{c}{\delta} \left(1-
      e^{-\delta(T-t^*)} \right) \\
    1 - \frac{\delta}{a} = & e^{-\delta(T-t^*)} \\
    t^* = & T + \frac{1}{\delta} \log\left( 1 - \frac{\delta}{a} \right).
  \end{align*}  
\end{solution}

\clearpage

{\footnotesize{ 
Definitions and results\footnote{Some of the results are stated
  without all the needed assumptions. If an assumption is not stated,
  you don't need to worry about it on the exam, but this list
  shouldn't be used as a rigorous reference.}
\begin{itemize}
\item  $f: X \to Y$ is \textbf{continuous} at $x$ if and only if 
  \begin{itemize}
  \item $\forall$ $\epsilon>0$ $\exists$ $\delta >0$ such that $d(x,x') < \delta $
    implies $d(f(x),f(x')) < \epsilon$; or
  \item $\forall x_n \to x$, $f(x_n) \to f(x)$; or
  \item $\forall$ open $U \subseteq Y$, $f^{-1}(U)$ is open
  \end{itemize}
\item $f:\R^n \to R$. the \textbf{partial derivative} of $f$ is 
  \[ \frac{\partial f}{\partial x_i} (x_0) = \lim_{h \to 0}
  \frac{f(x_{01},...,x_{0i}+h, ... x_{0n}) - f(x_0) }{h}. \]
\item $f: V \to W$, the (total) \textbf{derivative} of $f$ at $x_0$ is a  linear mapping, $Df_{x_0}:
  V \to W$ such that
  \begin{align*}
    \lim_{h \to 0} \frac{\norm{f(x_0 + h) - f(x_0) - Df_{x_0} h}} {\norm{h}}
    = 0.
  \end{align*}
\item \textbf{Mean value theorem}: $f:\R^n \to \R^m$ be continuously
  differentiable on some open $U$. Let $x, y \in U$ be such that the
  line connecting $x$ and $y$, is also in $U$, then $\exists
  \bar{x}_j$ on the line connecting $x$ and $y$ such that
  \[ f(x) - f(y) = \begin{pmatrix} D{f_1}_{\bar{x}_1} \\
    \vdots \\
    D{f_m}_{\bar{x}_m} \end{pmatrix} (x-y). 
  \] 
\item \textbf{Chain rule} if $f:V \to W$, $g: Z \to V$ and each is
  continuously differentiable on an open set, then so is $f \circ g$,
  and $D(f\circ g)_x = Df_{g(x)} Dg_x$
\item \textbf{Inverse function theorem}: if $f: \R^n \to \R^n$ is
  continuously differentiable on an open set containing $x_0$ and
  $Df_{x_0}$ is invertible, then $f$ is one-to-one on some open set
  containing $x_0$, so $f^{-1}$ exists on this set, and
  $D(f^{-1})_{f(x_0)} = (Df_{x_0})^{-1}$
\item \textbf{Implicit function theorem}: if $f:\R^{n+m} \to \R^n$
  continuously differentiable, $f(x_0,y_0) = c$, and
  $D_xf_{(x_0,y_0)}$ is invertible, then on some open set containing
  $x_0, y_0$, for each $y$ there is a unique $x$ such that $f(x,y) =
  c$, this defines a function $g(y) = x$, and $Dg_{y_0} =
  -\left(D_xf_{(x_0,y_0)}\right)^{-1} D_yf_{(x_0,y_0)}$
\item $f: V \to W$ is a \textbf{contraction} if $\norm{f(x) - f(y)}
  \leq c \norm{x-y}$ for some $0 \leq c < 1$. 
  \begin{itemize}
  \item If $f:V \to V$ is a contraction,  then $\exists$ a unique
    $x^*$ such that $f(x^*) = x^*$
  \end{itemize}
\item \textbf{First order condition}: if $f: V \to \R$ is
  differentiable and has a local maximum or minimum at $x$, then $Df_x
  = 0$
\item A matrix, $A$, is symmetric matrix is negative (positive) definite if $x^T A x
  < 0$ ($>0$).
\item If $A v = \lambda v$, then $v$ is an eigenvector and $\lambda$
  is an eigenvalue of $A$
\item If $f$ is twice continuously differentiable, $Df_x  = 0$ and
  $D^2 f_x$ is negative (positive) definite, then $x$ is a local
  maximizer (minimizer) 
\item If $f$ has a local maximum or minimum at $x^*$ subject to the
  constraints that $g(x) \leq b$, then 
  \begin{align*}
    \frac{\partial f}{\partial x_i} - {\lambda^*}^T \frac{\partial g}{\partial
      x_i}(x^*) = & 0 \\
    \lambda_j^* \left(g(x^*) - b \right)= & 0 \text{ with }
    \lambda_j^* \geq 0 \text{ and }  g(x^*) \leq b
  \end{align*}
\item \textbf{Pontryagin's maximum principle}: If $x^*,y^*$ solve
  \begin{align*}
    \max_{x,y \in U \subseteq X \times Y} & \int_0^T F(x(t),y(t),t) dt \notag \\
    \text{ s.t.} 
    &  \frac{d y}{dt} = g(x(t),y(t),t) \forall t \in
    [0,T] 
    & y(0) = y_0. \notag
  \end{align*}
  then let $H(x,y,\lambda,t) = F(x(t),y(t),t) + \lambda(t)
  g(x(t),y(t),t)$, and we will have
  \begin{align*}
    [x]: && 0 = & \frac{\partial H}{\partial x}(x^*,y^*,\lambda^*,t)
    \\
    [y]: && -\frac{d\lambda}{dt}(t) = & \frac{\partial H}{\partial y}(x^*,y^*,\lambda^*,t) \\
    [\lambda]: && \frac{dy}{dt}(t) = & \frac{\partial H}{\partial
      \lambda}(x^*,y^*,\lambda^*,t)   
  \end{align*}
\item \textbf{Integration by parts}: $\int_a^b u(t) \frac{dv}{dt}(t)
  dt = u(b)v(b) - u(a)v(a) - \int_a^b \frac{du}{dt}(t) v(t) dt$
\end{itemize}
}}


\end{document}
