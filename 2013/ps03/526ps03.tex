\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage[normalem]{ulem}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{problem}{Problem}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 
\def\F{\mathbb{F}}
\def\R{\mathbb{R}}
\newcommand{\gmatrix}[1]{\begin{pmatrix} {#1}_{11} & \cdots &
    {#1}_{1n} \\ \vdots & \ddots & \vdots \\ {#1}_{m1} & \cdots &
    {#1}_{mn} \end{pmatrix}}
\newcommand{\iprod}[2]{\left\langle {#1} , {#2} \right\rangle}
\renewcommand{\det}{\mathrm{det}}
\newcommand{\rank}{\mathrm{rank}}
\newcommand{\norm}[1]{ {\left\Vert {#1} \right\Vert} }

\title{Problem Set 3}
\date{Due: Monday, September 30th}

\begin{document}
\renewcommand{\theenumi}{\alph{enumi}}

\maketitle

\section{Limits and topology}
You may skip one of the problems in this section. You should at least
read each of them though because we might use the result later.

\begin{problem}
  Prove the following theorem from the lecture notes. Let $\{x_n \}$
  be a sequence in a normed vector space with scalar field $\R$ and
  let $\{c_n\}$ be a sequence in $\R$. If $x_n \to x$ and $c_n \to c$
  then
  \[ x_n c_n \to x c. \] 
  \emph{Hint: This is very similar to the theorem about $x_n + y_n \to
    x + y$ in lecture 6.}
\end{problem}

\begin{problem}[SB 12.17]
  Show that if $ x \in \mathrm{int}(S)$ and $\{x_n\}$ is a sequence
  converging to $x$, then $\exists N$ s.t. $x_n \in S$ for all $n \geq
  N$. This fact is sometimes used to define the interior of a set, and
  then open sets can be defined as sets that are equal to their
  interior.

  \emph{Hint: $\mathrm{int}(S)$ is open, so $\exists
    \epsilon>0$ such that $N_{\epsilon}(x) \subset
    \mathrm{int}(S)$. Then use the definition of a limit to show $x_n
    \in N_{\epsilon}(x)$ for large enough $n$.}
\end{problem}

\begin{problem}[SB 12.24]
  We generalize the definition of a limit point of a sequence to sets
  by defining $x$ to be a limit point\footnote{SB calls this an
    accumulation point of a set, but I think that is not very common
    terminology.} of $S$ if every neighborhood of $x$ contains points
  in $S$ other than $x$. Show that if $S$ is closed, then it contains
  all its limit points.\footnote{This result is sometimes used as the
    definition of closed sets.} For any set, $S$, show that
  $\overline{S}$ is the union of $S$ and its limit points.
  
  \emph{Hint: let $x$ be a limit point of $S$. Construct a sequence,
    $\{x_n\}$, in $S$ that converges to $x$ be taking $x_n \in
    N_{1/n}(x) \cap S$. Then use the theorem from lecture 6 that
    showed if $S$ is closed and $\{x_n\} \in S$ converges to $x$, then
    $x \in S$. }
\end{problem}

\begin{problem}
  Let $X$ be a metric space and let $A \subseteq B \subseteq X$. $A$
  is \textbf{dense} in $B$ if $\overline{A} = B$, or, equivalently, if
  for each $x \in B$ and $\epsilon>0$ $\exists y \in A \cap
  N_{\epsilon}(x)$. A set is called \textbf{separable} if it contains
  a countable dense subset. Prove that compact sets are
  separable.
  
  \emph{Hint: for $n=1,2,...$ consider open covers of the
    form $N_{1/n}(x)$ for $x \in B$. For each $n$ there exists a
    finite subcover, say $N_{1/n}(x_{n1}), ..., N_{1/n}(x_{nk_n})$. Let
    $A = \{x_{11}, ..., x_{1k_1}, x_{21} , ...  \}$.}
\end{problem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\section{Functions}
You may skip one of the problems in this section. 

\begin{problem}
  Let $A$ and $B$ be metric spaces and $f:A \to B$. Prove that $f$ is
  continuous at $x$ if and only if $\forall$ $\epsilon > 0$ $\exists$
  $\delta > 0$ such that $d(x,x')< \delta$ implies $d(f(x), f(x')) <
  \epsilon$. In the lecture notes, we defined continuity in terms of
  sequences, but the statement in this problem is often used as the
  definition of continuity instead. 
  \emph{Hint: If $f$ is continuous and $x_n \to x$, then by
    definition, $f(x_n) \to f(x)$. Suppose $f$ is continuous and there
    is an $\epsilon>0$ such that such that for any $\delta>0$,
    $d(x,x') < \delta$ does not imply $d(f(x),f(x'))< \epsilon$. Get a
    contradiction by constructing a sequence $x_n \to x$, but with
    $d(f(x_n),f(x)) \geq \epsilon$. To show the other direction, let
    $x_n \to x$. Then for $n\geq N$ $d(x_n,x) <\delta$, and
    $d(f(x_n),f(x))<\epsilon$.}
\end{problem}

\begin{problem}
  Let $A$ and $B$ be metric spaces and $f:A \to B$. $f$ is
  open continuous\footnote{Not standard terminology.} if for any open
  $V \subseteq B$, $f^{-1}(V)$ is open. $f$ is closed
  continuous\footnote{Not standard terminology.} if
  for any closed $C \subseteq B$, $f^{-1}(C)$ is closed. 
  \begin{enumerate}
  \item Prove that a function is closed continuous if and only if it
    is open continuous. \\
    \emph{Hint: this should be short. Use the fact that if $U$ is
      open, then $U^c$ is closed.}
  \item Prove that a function is continuous if and only if it is open
    continuous. \\
    \emph{Hint: Let $V \subset B$ be open and $f(x) \in V$. Then
      $\exists \epsilon>0$ such that $N_{\epsilon}(f(x)) \subset
      V$. From the previous problem, $\exists \delta>0$ such that
      $d(x,x') < \delta$ implies $d(f(x),f(x'))<\epsilon$. Conclude
      that $N_\delta(x) \subset f^{-1}(V)$.}
  \end{enumerate}
\end{problem}

\begin{problem}
  Let $A$ and $B$ be metric spaces and $f:A \to B$. $f$ is
  \textbf{uniformly continuous} on $A$ if for any $\epsilon
  > 0$ there exists a \emph{single} $\delta > 0$ such that \emph{for
    all} $x, x' \in A$ with $d(x,x') < \delta$  we have
  $d(f(x),f(x'))<\epsilon$. 
  \begin{enumerate}
  \item Let $A = (0,1)$ and $B = \R$. Is $f(x) = 1/x$ continuous? Is
    $f$ uniformly continuous? 
  \item Let $A = (0,1)$ and $B = \R$. Is $f(x) = x^2$ continuous? Is $f$
    uniformly continuous?    
  \item Let $A = \R$ and $B = \R$. Is $f(x) = x^2$ continuous? Is $f$
    uniformly continuous?
  \item Let $A$ be compact and $f:A\to B$ be continuous. Prove that
    $f$ is uniformly continuous.   \\
    \emph{Hint: Let $\epsilon>
      0$. $f^{-1}(N_\epsilon(y))$ for $y \in B$ is an open cover of
      $A$, so there is a finite subcover, say
      $f^{-1}(N_\epsilon(y_1)), ..., f^{-1}(N_\epsilon(y_k))$. Let
      $f(x_i) = y_i$. $f$ is continuous at each $x_i$ so $\exists$
      $\delta_i>0$ such that $d(x_i,x)<\delta_i$ implies
      $d(f(x_i),f(x)) <\epsilon$. Use the triangle inequality to
      conclude that $d(f(x),f(x')) < \epsilon$ whenever $d(x,x') <
      \frac{1}{2} \min_{i\in\{1,...,k\}} \delta_i$.}
  \end{enumerate}
\end{problem}

\end{document}
