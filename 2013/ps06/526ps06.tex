\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage[normalem]{ulem}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{problem}{Problem}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 
\def\F{\mathbb{F}}
\def\R{\mathbb{R}}
\newcommand{\gmatrix}[1]{\begin{pmatrix} {#1}_{11} & \cdots &
    {#1}_{1n} \\ \vdots & \ddots & \vdots \\ {#1}_{m1} & \cdots &
    {#1}_{mn} \end{pmatrix}}
\newcommand{\iprod}[2]{\left\langle {#1} , {#2} \right\rangle}
\renewcommand{\det}{\mathrm{det}}
\newcommand{\rank}{\mathrm{rank}}
\newcommand{\norm}[1]{ {\left\Vert {#1} \right\Vert} }

\title{Problem Set 6}
\date{Due: Monday, November 4th}

\begin{document}
\renewcommand{\theenumi}{\roman{enumi}}

\maketitle

\begin{problem}[Contracting with asymmetric information]
  \footnote{This problem is based on chapter 2 of \textit{Contract
      theory} by Bolton and Dewatripont. Feel free to consult that
    book if you are having difficulty.}Consider a monopolist selling
  a good to two types of consumers, high 
  demand and low demand. High types get utility 
  \[ \theta_h v(q) - T \]
  from consuming $q$ units of the good at cost $T$, where
  $v(q)>0$ and $v'(q)>0$. Similarly, low types get utility 
  \[ \theta_l v(q) - T \]
  where $\theta_l < \theta_h$. 
  The portion of low types in the population is $\beta$ and the
  portion of high types is $1-\beta$. Assume that both types of
  consumer get utility $0$ from not consuming. Also assume that
  there is no secondary market for the good. That is, consumers can only
  buy the good from the monopolist; they cannot trade amongst
  themselves. It costs the seller $cq$ to produce $q$ units of the
  good. 
  \begin{enumerate}
  \item\label{fb}\textbf{[First best]} Suppose the seller observes the types of
    the consumer and so 
    can charge them different prices. The seller maximizes profits subject
    to the consumers actually buying the good.
    \begin{align*}
      \max_{q_l,T_l,q_h,T_h} \beta(T_l - c q_l) + (1-\beta)(T_h - c
      q_h) \text{ s.t. } & \theta_l v(q_l) - T_l \geq 0 \\
      & \theta_h v(q_h) - T_h \geq 0
    \end{align*}
    write down the first order condition.
  \item\textbf{[Linear pricing]} Now suppose the seller cannot observe
    the types of the 
    consumers. Suppose the seller charges a constant linear price,
    $p$, so $T_l = p q_l$ and $T_h = p q_h$. Let $D_l(p)$ be the
    demand function for low types and $D_h(p)$ be the demand function
    for high types. In other words, 
    \[ D_i(p) = \argmax_q \theta_i v(q) - p q. \]
    Assume that $D_h'(p)<0$.
    \begin{enumerate}
    \item Write the firm's and consumer's first order conditions for
      $p$.
    \item Show that consumption is lower here than in part (\ref{fb}). 
    \item Let $S(q,T) = \beta(\theta_l v(q_l) - cq_l) + (1-\beta)
      (\theta_h v (q_h) - c q_h)$ be the total surplus. Show that the
      total surplus is lower here than in \ref{fb}.
    \end{enumerate}
  \item\textbf{[Two-part tariffs]} 
    %\textit{You do not have to hand in your answer to this part.} \\
    Suppose the seller charges a two-part tariff
    $T(q) = z + pq$. Assume that $z$ is small relative to consumer
    income, so there is no income effect and demand only depends on
    $p$ provided utility from consuming and paying $z + D(p) p$
    exceeds utility from not consuming. Then the profit maximization
    problem assuming both types of consumers purchase something is:
    \begin{align*}
      \max_{z,p} \beta\left(z+pD_l(p) - c D_l(p)\right) + (1-\beta)
      \left( z + p D_h(p) - c D_h(p) \right) \text{ s.t. } & \theta_l
      v(D_l(p)) - (z + p D_l(p)) \geq 0 \\
      & \theta_h v(D_h(p)) - (z + p D_h(p)) \geq 0 \\
    \end{align*}
    \begin{enumerate}
    \item Use the envelope theorem on 
      \[ \max_q \theta_i v(q) - (z+pq) \]
      to show that $\theta_h v(D_h(p)) -  (z + p D_h(p)) > \theta_l
      v(D_l(p)) - (z + p D_l(p))$.  This implies that only the first
      constraint in the seller's problem can be binding.
    \item Write the seller's first order condition. Use the fact that
      only the first constraint binds to show that $p^*>c$ and there
      again consumption is lower than in (\ref{fb}).
    \end{enumerate}
  \item\textbf{[Second-best pricing]} Now suppose the seller can offer
    any pricing schedule, $T(q)$. This could be any function, perhaps
    very complicated. However, an important insight called the
    revelation principle is that the only thing that matters about
    $T(q)$ is $T_l=T(q_l)$ and $T_h = T(q_h)$ where $q_l$ and $q_h$
    are the quantities actually chosen by types $l$ and $h$. Thus, we
    can just focus on these two quantities
    \begin{align} 
      \max_{T_i,q_i} \beta(T_l - cq_l) +(1-\beta)(T_h - cq_h) 
      \text{s.t.} & \\
      \theta_h v(q_h) - T_h \geq & \theta_h v(q_l) - T_l \label{ich}
      \\
      \theta_l v(q_l) - T_l \geq & \theta_l v(q_h) - T_h \label{icl}
      \\
      \theta_h v(q_h) - T_h \geq & 0 \label{irh} \\
      \theta_l v(q_l) - T_l \geq & 0 \label{irl}
    \end{align}
    Constraints (\ref{ich}) and (\ref{icl}) are called incentive
    compatibility constraints. They say that type $i$ must prefer the
    contract designed for type $i$ to the contract for the other
    type. Constraints (\ref{irh}) and (\ref{irl}) are called
    individual rationality or participation constraints. They simply
    say that type $i$ should prefer buying the good to not buying it.
    \begin{enumerate}
    \item Argue that \ref{irh} will not bind because it is implied by
      \ref{irl} and \ref{ich}
    \item Guess that \ref{icl} also does not bind at the
      maximum. Write down the first order conditions and verify that 
      \ref{icl} indeed does not bind. 
    \item Show that $q_h$ is the same as in (\ref{fb}), but $q_l$ is
      lower. 
    \end{enumerate}     
  \item The basic structure of this problem can be applied to many
    other settings such as credit rationing and income
    taxation. Explain what $\theta$, $v$, $q$ and $T$ might be
    interpreted as in one of these other settings.
  \end{enumerate}
\end{problem}

\begin{problem}
  Suppose a firm has $q_0$ units of a non-renewable resource. Let the
  firm's inverse instantaneous demand curve be $p(x(t))$. That is, at
  instant $t$ if the firm sells $x(t)$, then the price will be
  $p(x(t))$. The firm's problem is
  \begin{align*}
    \max_{x,q} & \int_0^T  e^{-\alpha t} x(t) p(x(t)) dt &
    \text{ s.t. } \frac{dq}{dt} &= -x(t) \\
    && q(0) &= q_0 \\
    && q(T) &\geq 0
  \end{align*}
  \begin{enumerate}
  \item Write the first order condition for the firm's problem.
  \item Suppose $p(x) = 1 - \frac{x}{2}$ for $0 \leq x \leq 2$ and
    $p(x) = 0$ for $x>2$. 
    \begin{enumerate}
    \item Show that $\lambda(t) = \lambda$ is constant.
    \item Solve for $\lambda$ using the boundary constraint(s).
    \item Solve for $x(t)$.
    \item Solve for $q(t)$. 
    \end{enumerate}
  \end{enumerate}
\end{problem}

\begin{problem}
  Suppose an unemployed worker receives a wage offer independently
  distributed uniformly on $[0,\bar{w}]$ each period. If the worker
  accepts the offer, the worker receives the wage forever. If the
  worker declines the offer, the worker receives unemployment benefits
  of $b$ and continues to be unemployed next period. The worker
  discounts the future at rate $\beta$, and has instantaneous utility
  equal to wages or unemployment benefits.
  \begin{enumerate}
  \item Find the utility from accepting a wage of $w$. \textit{Hint: this
      is short.}
  \item Write the Bellman equation for the value of receiving a wage
    offer of $w$. \\
    \textit{Hint: There are multiple ways to set this up. One way is
      to let $V(w)$ be the value of getting an offer of $w$. Given an
      offer of $w$, you can either accept it and 
      get your answer from the previous part, or reject it and get $b$
      plus the discount factor times the expectation of $V(w)$ next
      period, i.e.\ $\beta \int_{0}^{\bar{w}} V(w') \frac{1}{\bar{w}} dw'$ }
  \item Argue that the optimal strategy must be to accept all wages
    above some reservation wage $w^r$. \\
    \textit{Hint: the value of rejecting a wage offer does not depend
      on the offered wage.}
  \item Given the reservation wage strategy, explain why you can write
    the value of rejecting a wage offer as
    \[ v_0 = b + \beta \left(\int_{w^r}^{\bar{w}} \frac{w'}{1-\beta}
       \frac{1}{\bar{w}} dw' + \frac{w^r}{\bar{w}} v_0 \right). \]
  \item The reservation wage must be such that $ v_0 =
    \frac{w^r}{1-\beta}.$ Use this fact to calculate $\frac{\partial
      w^r}{\partial b}$.  
  \end{enumerate}
\end{problem}

\end{document}
