\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{enumitem}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage{ulem}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{problem}{Problem}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}
\usepackage{natbib}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}
\def\R{\mathbb{R}}
\newcommand{\norm}[1]{\left\Vert {#1} \right\Vert}
\newcommand{\abs}[1]{\left\vert {#1} \right\vert}
\renewcommand{\det}{\mathrm{det}}
\newcommand{\rank}{\mathrm{rank}}
\newcommand{\spn}{\mathrm{span}}
\newcommand{\row}{\mathrm{Row}}
\newcommand{\col}{\mathrm{Col}}
\renewcommand{\dim}{\mathrm{dim}}
\newcommand{\prefeq}{\succeq}
\newcommand{\pref}{\succ}
\newcommand{\seq}[1]{\{{#1}_n \}_{n=1}^\infty }
\renewcommand{\to}{{\rightarrow}}


\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\title{Problem Set 7}
\date{Due: Wednesday, November 5th}

\begin{document}

\maketitle

\begin{problem}
  Consider a financial model with $A$ assets and $S$ states of
  nature. States of nature are things about which we are
  uncertain. You could think of the assets as various stocks and the
  states of nature could be all possible combinations of prices of the
  stocks tomorrow. For this problem, we will assume $A$ and $S$ are
  finite so that everything can be represented by matrices, but we
  could allow them to be infinite and use abstract linear
  transformations instead.  Considering an infinite number of assets
  is a bit of a stretch, but you could imagine bonds that mature at
  time $t$, $t+1$, $t+2$, and so on forever. Of course, in the real
  world we don't see bonds that mature arbitrarily far into the
  future. On the other hand, allowing for infinite states of nature
  $S$ is pretty reasonable. In the model a state of nature is anything
  that affects the value of the assets. Each potential combination of
  values of the assets is a single state of nature.

  We will think about a single period model. At the start of the
  period, the value of asset $i \in \{1,..,A\}$ is $v_i$. Then some
  state of nature, $s \in \{1, ..., S\}$, is drawn and the value of
  the asset becomes $y_{si}$. The realized return of asset $i$ in
  state $s$ is $r_{si} = \frac{y_{si}}{v_i}$. Let $R$ be the
  $S$ by $A$ matrix consisting of the $r_{si}$.

  The investor has wealth $w_0$ is choosing to buy $n_{i}$ units or
  shares of asset $i$. The budget constraint is
  \begin{align}
    \sum_{i=1}^A n_i v_i = w_0
  \end{align}
  Let $x_i = \frac{n_i v_i}{w_0}$ be the share of wealth in asset
  $i$. We call $(x_1, ..., x_A)$ a portfolio. 
  The return to a portfolio $x = (x_1,...,x_A)$ in state $s$ is
  \begin{align}
    r_s = \sum_{i=1}^A \frac{y_{si}}{v_i} x_i = \sum_{i=1}^A r_{s} x_i 
  \end{align}
  \begin{enumerate}
  \item A portfolio is \textbf{riskless} if $x \neq 0$ and
    \begin{align*}
      \sum_{i=1}^A r_{1i} x_i  =  \cdots   =\sum_{i=1}^A r_{Si} x_i = c
    \end{align*}
    for some constant $c$.  What conditions on the range and null
    space of $R$ guarantee existence of a riskless portfolio? 
  \item  A state $s^*$ is \textbf{insurable} if $\exists$ portfolio $x^*$ such
    that $\sum_{a=1}^A r_{s^*a} x_a^* > 0$ and $\sum_{a=1}^A r_{sa} x_a^* =
    0$ for all $s \neq s^*$. What conditions on the range and null
    spaces of $R$ guarantee that state $s^*$ is insurable? When are
    all states insurable?
  \item A portfolio $x$ is \textbf{duplicable} if there is
    another portfolio $w$ such that $\sum_{i=1}^A x_i = \sum_{i=1}^A w_i
    $ and 
    \[ R x = R w. \]
    Under what conditions on the range and null spaces of $R$ will
    duplicable portfolios exist?
  \item Suppose there are asset prices $p = (p_1, ..., p_A)$ with each
    $p_a \geq 0$. An \textbf{arbitrage} is a portfolio such that $p^T
    x < 0$ and $R x \geq 0$ (for vectors, $v \in \R^S$, $v \geq 0$
    means $v_i \geq 0$ for $i = 1, ..., n$). Use Farkas' lemma (stated
    below) to show that there is no arbitrage if and only
    if there exists state prices $\pi = (\pi_1, ..., \pi_S)$ with
    $\pi_s \geq 0$, such that $p = R^T \pi$.
  \end{enumerate}
  
  \begin{lemma}[Farkas' lemma]
    Let $A$ be an $m$ by $n$ matrix and $b \in \R^m$. Then either:
    \begin{itemize}
    \item[(i)] $\exists x = (x_1, ..., x_n) \in \R^n$ such that $x_1
      \geq 0, ..., x_n \geq 0$ and $Ax = b$ or
    \item[(ii)] $\exists y \in \R^m$ such that $y^T A e_i \geq 0$ for
      each standard basis vector $e_i \in \R^n$ and $y^T b < 0$
    \end{itemize}
    but not both. 
  \end{lemma}
  Farkas' lemma is a consequence of the separating hyperplane
  theorem, which we will hopefully get to cover, but might not.
\end{problem}

\begin{problem}\label{contLin}
  Let $V$ and $W$ be normed vector spaces and $A: V \to W$ be
  linear. We $A$ is \textbf{continuous} at $x$, if for every $\epsilon>0$
  $\exists \delta >0$ such that for all $y \in V$ with $\norm{x-y} <
  \delta$ we have $\norm{A x - Ay } < \epsilon$.
  \begin{enumerate}
  \item Prove that if $A$ is continuous at $0$, then $A$ is continuous
    at all $v \in V$. 
  \item $A$ is \textbf{bounded} if there exists a constant $M$ such
    that
    \[ \norm{A v} \leq M \norm{v} \] 
    for all $v \in V$. Prove that $A$ is continuous (at all $v \in V$)
    if and only if $A$ is bounded.
  \item Show that if $V$ is finite dimensional, then $A$ is
    continuous.
  \item Give an example of a discontinuous linear transformation. 
    % \emph{Hint: do not spend too much time on this part; it will be
    % worth at most 1 point.}
  \end{enumerate}  
\end{problem}

\begin{problem}
  Let $X$ be a metric space and $\succeq$ be a preference relation on
  $X$. The preference relation is continuous if the sets $\succeq(y) =
  \{x : x \succeq y \}$ and $\preceq(y) = \{x : x \preceq y \}$ are
  closed\footnote{Metric spaces and closed sets are the next topic we
    will cover. This problem can be solved without having any idea
    what metric space or a closed set is.} for every $y$. Assume that
  $B \subseteq X$ is such that for any collection of closed subsets of
  $B$, $\{C_\alpha \}_{\alpha \in \mathcal{A}}$ such that
  $\cap_{\alpha \in F} C_\alpha$ is not empty for any finite $F
  \subset \mathcal{A}$ then $\cap_{\alpha \in \mathcal{A}} C_\alpha$
  is not empty.\footnote{we will soon see that this is equivalent to
    $B$ being compact.} Show that if $\succeq$ is continuous, then $B$
  has a best element, i.e.\ $\exists x^* \in B$ such that $x^* \succeq
  y$ for all $y \in B$.
\end{problem}

%\bibliographystyle{jpe}
%\bibliography{../../526}

\end{document}
