\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{mdframed}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage{ulem}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 
\def\R{\mathbb{R}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}

\newtheorem{prob}{Problem}
\newtheorem{solution}{Solution to problem}
\newenvironment{problem}
  {\begin{mdframed}\begin{prob}}
      {\end{prob}\end{mdframed}}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\title{Problem Set 3}
\date{Due: Monday, October 17th}

\begin{document}

\maketitle

\begin{problem}
  Consider the problem
  \[ \max_{\{c_t\}_{t=0}^\infty, \{s_t\}_{t=1}^\infty} \sum_{t=0}^\infty \beta^t \log(c_t)
    \text{ s.t. } s_{t+1} = (1+r)(s_t - c_t) \]
  \begin{enumerate}
  \item State the Bellman equation for this problem.
  \item Solve for the value function.
  \item Now suppose that instead of the interest rate, $r$, being constant,
    the interest rate is random. Each period the $r$ is one of $K$
    values, $r_1, ..., r_k$.  Independently every period, $r = r_i$
    with probability $p_i$. The state variables are now $s$ and $r$,
    and the Bellman equation can be written as
    \[ V(r, s) = \max_{c,s'}\log(c) + \beta \sum_{i=1}^K p_i V(r_i, s')
      \text{ s.t. } s' = (1+r)(s - c) \]
    Solve for $V(r,s)$. {\slshape{Hint: a good guess for the
        functional form of the value function is $V(r_i,s) = A
        \log(s) + B(r_i)$. Use this guess and the Bellman equation to
        solve for $A$ and $B(r)$.}}
  \end{enumerate}
\end{problem}
\begin{solution}$\;$
  \begin{enumerate}
  \item
    \[ V(s) = \max_{c,s'} \log(c) + \beta V(s') \text{ s.t. }
      \text{ s.t. } s_{t+1} = (1+r)(s_t - c_t) \]
  \item Based on the hint for the next part, or when we solved this
    problem in class, let's guess that the form of the value function
    is $V(s) = A \log(s) + B$.
    The first order condition for the Bellman equation is then
    \[ \frac{1}{c^*} = \frac{\beta A(1+r)}{(1+r)(s-c^*)} \]
    so
    \[ c^* = \frac{s}{1+A\beta} \]
    Plugging back into the Bellman equation
    \begin{align*}
      A \log (s) + B = & \log(s/(1+A\beta)) + \beta \left(A \log(A\beta(1+r)
                         s/(1+A\beta)) + B \right) \\
      = & (1+A\beta)\log(s) + A\beta \log(A\beta(1+r)) - (1+ A
          \beta)\log(1+A\beta) + \beta B
    \end{align*}
    so $A = \frac{1}{1-\beta}$, and
    \[ B = \frac{1}{1-\beta}\left(A\beta \log(A\beta(1+r)) - (1+ A
        \beta)\log(1+A\beta) \right). \]
  \item With the suggested guess for the value function, the first
    order condition is
    \[ \frac{1}{c^*} = \sum_{j=1}^K p_i \frac{\beta A(1+r_i)}{(1+r_i)(s-c^*)} \]
    so
    \[ c^*(r_i) = \frac{s}{1+A\beta}. \]
    We wrote $c^*(r_i)$ to emphasize that $c^*$ could have depended on
    $r_i$, even though in this particular case it did not. Anyway,
    putting $c^*(r_i)$ into the Bellman equation gives
    \begin{align*}
      A \log (s) + B(r) = & \log(s/(1+A\beta)) + \beta \sum_{j=1}^K
                            p_j \left(A \log(A\beta(1+r)
                            s/(1+A\beta)) + B(r_j) \right) \\
      = & (1+A\beta)\log(s) + A\beta \log(A\beta(1+r)) - (1+ A
          \beta)\log(1+A\beta) + \beta  \sum_{j=1}^K p_j \log(B(r_j))
    \end{align*}
    Again, we have $A = \frac{1}{1-\beta}$. Also,
    \[ \sum_{i=1}^K p_i B(r_i) = \sum_{i=1}^K \left(p_i A\beta \log(A\beta(1+r_i)) - (1+ A
        \beta)\log(1+A\beta) + \beta  \sum_{j=1}^K p_j \log(B(r_j))
      \right) \]
    so
    \[ \sum_{i=1}^K p_i B(r_i)  = \frac{1}{1-\beta} \left(A \beta
        \log(A\beta) - (1+ A \beta)\log(1+A\beta) + A\beta
        \left[\sum_{i=1}^K p_i \log(1+r_i)\right]\right) \]
    and
    \[ B(r_i) = \frac{1}{1-\beta} \left(A \beta
        \log(A\beta) - (1+ A \beta)\log(1+A\beta) + A\beta
        \log(1+r_i) \right) \] 
  \end{enumerate}
\end{solution}


\begin{problem}
  Suppose an unemployed worker receives a wage offer independently
  distributed uniformly on $[0,\bar{w}]$ each period. If the worker
  accepts the offer, the worker receives the wage forever. If the
  worker declines the offer, the worker receives unemployment benefits
  of $b$ and continues to be unemployed next period. The worker
  discounts the future at rate $\beta$, and has instantaneous utility
  equal to wages or unemployment benefits.
  \begin{enumerate}
  \item Find the utility from accepting a wage of $w$. \textit{Hint: this
      is short.}
  \item Write the Bellman equation for the value of receiving a wage
    offer of $w$. \\
    \textit{Hint: There are multiple ways to set this up. One way is
      to let $V(w)$ be the value of getting an offer of $w$. Given an
      offer of $w$, you can either accept it and 
      get your answer from the previous part, or reject it and get $b$
      plus the discount factor times the expectation of $V(w)$ next
      period, i.e.\ $\beta \int_{0}^{\bar{w}} V(w') \frac{1}{\bar{w}} dw'$ }
  \item Argue that the optimal strategy must be to accept all wages
    above some reservation wage $w^r$. \\
    \textit{Hint: the value of rejecting a wage offer does not depend
      on the offered wage.}
  \item Given the reservation wage strategy, explain why you can write
    the value of rejecting a wage offer as
    \[ v_0 = b + \beta \left(\int_{w^r}^{\bar{w}} \frac{w'}{1-\beta}
       \frac{1}{\bar{w}} dw' + \frac{w^r}{\bar{w}} v_0 \right). \]
  \item The reservation wage must be such that $ v_0 =
    \frac{w^r}{1-\beta}.$ Use this fact to calculate $\frac{\partial
      w^r}{\partial b}$.  
  \end{enumerate}
\end{problem}
\begin{solution}
  $\;$
  \begin{enumerate}
  \item If you accept a wage of $w$, you get 
    \[ \sum_{t=0}^\infty \beta^t w = \frac{w}{1-\beta}. \]
  \item Let $V(w)$ denote the value of receiving a wage offer of
    $w$. You can either accept it and get $w/(1-\beta)$, or reject and
    get $b$ plus a new offer next period. That is,
    \[ V(w) = \max\left\lbrace \frac{w}{1-\beta}, b + \beta
    \int_0^{\bar{w}} V(w') dw'/\bar{w} \right\rbrace .  \] 
    I left the division by $\bar{w}$ out of the hint, so I didn't take
    off any points for omitting it.
  \item As the hint states, the value of rejecting a wage, 
    \[ V(\text{reject})= b + \beta \int_0^{\bar{w}} V(w') dw'/\bar{w}, \]
    does not depend on the wage offered today. Therefore,
    $V(\text{reject}) = v_0$ for some constant $v_0$. Hence, it must be
    that the wage is accepted whenever $\frac{w}{1-\beta} \geq
    v_0$. In other word, you accept the wage if $w \geq (1-\beta) v_0
    = w^r$.
  \item Given the reservation wage, $V(w)$ can be written as
    \[ V(w) = \begin{cases} \frac{w}{1-\beta} & \text{ if } w \geq w^r
      \\
      v_0 & \text{ if } w < w^r
    \end{cases} \]
    and 
    \[ v_0 = b + \beta \int_0^{\bar{w}} V(w')dw'/\bar{w}. \]
    Plugging in the previous formula for $V(w')$, we have
    \begin{align*}
      v_0 = & b + \beta \left(\int_0^{w^r} v_0 dw'/\bar{w} +
      \int_{w^r}^{\bar{w}} \frac{w'}{1-\beta} dw'/\bar{w}\right) \\
      = & b + \beta \left(v_0 \frac{w^r}{\bar{w}} + \frac{\bar{w}^2 -
        (w^r)^2} {2(1-\beta) \bar{w}} \right) 
    \end{align*}
  \item 
    From the previous part, 
    \begin{align*}
      v_0 = & \frac{ b + \beta \frac{\bar{w}^2 -
          (w^r)^2} {2(1-\beta) \bar{w}}} { 1 - \beta
        \frac{w^r}{\bar{w}} } \\
      = & \frac{ b\bar{w} + \beta \frac{\bar{w}^2 -
          (w^r)^2} {2(1-\beta)}} { \bar{w} - \beta
        w^r } \\ 
    \end{align*}  
    Thus, we have,
    \begin{align*}
      \frac{w^r}{1-\beta} = & v_0 \\
      = & \frac{ b\bar{w} + \beta \frac{\bar{w}^2 -
          (w^r)^2} {2(1-\beta)}} { \bar{w} - \beta
        w^r } \\
      w^r = & \frac{ b\bar{w}(1-\beta) + \beta \frac{\bar{w}^2 -
          (w^r)^2} {2}} { \bar{w} - \beta
        w^r } 
    \end{align*}      
    You could use the quadratic formula to explicit solve for $w^r$,
    or you can just use the implicit function theorem. 
    To use the implicit function theorem, write the previous equation as
    \begin{align*}
      w^r (\bar{w}-\beta w^r) + \frac{\beta}{2} \left((w^r)^2 -
        \bar{w}^2\right) - b \bar{w}(1-\beta) = & 0 \\
      F(w^r, b) = & 0
    \end{align*}      
    Then, 
    \begin{align*}
      \frac{\partial w^r} {\partial b} (b) = & - \left( \frac{\partial
          F}{\partial w^r} \right)^{-1} \frac{\partial F}{\partial b} \\
      = & - \left(\bar{w} - 2\beta w^r + \beta w^r \right)^{-1}
      \left(-\bar{w}(1-\beta)\right) \\
      = & \frac{\bar{w}(1-\beta)} 
      {\bar{w} - \beta w^r }
    \end{align*}
    Note that this is positive because $\bar{w} > w^r$ and $\beta <
    1$. Also, 
    \[ \bar{w}(1-\beta) < \bar{w} - \beta w^r, \]
    so $\frac{\partial w^r} {\partial b} < 1$. An increase in
    unemployment benefits increases the reservation wage, but by less
    than proportionally. 
  \end{enumerate}
\end{solution}


% \begin{problem}
%   Consider the following social choice function. If everyone agrees,
%   $x \succ_i y $ for all $i$, then $x \succ y$. Otherwise, we remove
%   the first person, and check if $x \succ_i y$ for $i = 2, ..., n$,
%   then $x \succ y$. Otherwise, we remove the next person, and check if
%   $x \succ_i y$ for $i = 3, ..., n$, and then $x \succ y$. We continue
%   in this way until everyone that is left agrees.
%   \begin{enumerate}
%   \item Does this complete the Pareto order?
%   \item Is this order dictatorial?
%   \item Is this order transitive?
%   \item Does this order obey the independence of irrelevant
%     alternatives?
%   \end{enumerate}
% \end{problem}
% \begin{solution}
%   \begin{enumerate}
%   \item Yes. If $x \succ_i y$ for all $i$,then $x \succ y$.
%   \item Yes. Person $n$ is the dictator. If $x \succ y$, then $x
%     \succ_n y$. Also if not $x \succ_n y$, then not $x \succ y$.
%   \item Yes, since each $\succ_i$, so is $\succ$.
%   \item Yes. Let $X$ be the set of all alternatives, and $A \subseteq
%     X$. If $x, y \in A$, and $\{\succ_i\}_{i=1}^n$ and
%     $\{\succ_i'\}_{i=1}^n$ are preferences that agree on $A$, but
%     differ elsewhere, then the social preference for $x$ and $y$ will
%     be the same for the two sets of preferences. Changing preferences
%     outside of $A$ (in fact changing preferences over outcomes other
%     than $x$ and $y$) has no effect on what the social choice rule
%     says about $x$ and $y$.
%   \end{enumerate}
% \end{solution}

% \begin{problem}
%   For each of the following sets in $\R^2$ state whether it is open,
%   whether it is closed, and whether it is compact. Briefly justify
%   your answer.
%   \begin{enumerate}
%   \item $A = \{(x,y): x^2 + y^2 < 1\} \cap \{(x,y): x>0 \}$
%   \item $B = \{(x,y): x- 3 y = 20\}$
%   \item $C = B \cap \{(x,y): x^2 + y^2 \leq 346 \}$
%   \item $D = \cup_{i=1}^\infty D_i$, with $D_i = (-1+1/i,1-1/i) \times
%     (0,1)$\footnote{That is, $D_i = \{(x,y): -1+1/i < x < 1-1/i, 0<y<1\}$}
%   \item $E = \cup_{i=1}^\infty E_i$, with $E_i = [0,1-1/i] \times [0,1]$
%   \end{enumerate}
% \end{problem}
% \begin{solution}$\,$
%   \begin{enumerate}
%   \item It is open. The first set is $N_1(0)$, which is open, the
%     secod is a half open plane, we can find a neighborhood of
%     e.g.\ $N_{x/2}((x,y))$ for any any point in this set. So, $A$ is
%     the intersection of two open sets, so still open. It is not
%     closed, so it is also not compact.
%   \item This is a affine subspace, so it is closed. It is not open. It is
%     not bounded, so not compact.
%   \item $B$ is closed. The second set is a closed neighborhood of
%     zero, so it is also closed. Thus, $C$ is closed. $C$ is not open
%     because it contains no neighborhoods. $C$ is closed, and $C$ is
%     bounded (all points are at most $\sqrt{346}$ from zero). Thus,
%     $C$ is compact.
%   \item $D$ is the infinite union of open sets, so it is open. $D$ is
%     open and not the empty set or the whole space, so it not closed,
%     and therefore also not compact. 
%   \item $E = [0,1) \times [0,1]$ because for any $0\leq x < 1$, $x
%     \leq 1 - 1/i$ for large enough $i$ (this shows $E \subseteq
%     [0,1)\times [0,1]$), and all $E_i \subseteq [0,1) \times [0,1]$,
%     so it must be that $E \subseteq [0,1) \times [0,1]$, so $E = [0,1)
%     \times [0,1]$. Anyway, this set is neither closed nor open nor
%     compact. It is not open because it contains no neighborhoods of
%     0. It is not closed, because it contains the sequence
%     $\{(1-1/n,0)\}$, but not its limit, $(1,0)$.
%   \end{enumerate}
% \end{solution}

% \begin{problem}
%   Let $X$ be a metric space and let $A \subseteq B \subseteq X$. $A$
%   is \textbf{dense} in $B$ if $\overline{A} = B$, or, equivalently, if
%   for each $x \in B$ and $\epsilon>0$ $\exists y \in A \cap
%   N_{\epsilon}(x)$. A set is called \textbf{separable} if it contains
%   a countable dense subset. Prove that compact sets are
%   separable.
  
%   \emph{Hint: for $n=1,2,...$ consider open covers of the
%     form $N_{1/n}(x)$ for $x \in B$. For each $n$ there exists a
%     finite subcover, say $N_{1/n}(x_{n1}), ...,
%     N_{1/n}(x_{nk_n})$. Let
%     $A = \{x_{11}, ..., x_{1k_1}, x_{21} , ...  \}$.}
% \end{problem}
% \begin{solution}
%   Let $K$ be compact. Define $A$ is in the hint. $A$ is obviously
%   countable. Let $x \in K$ and $\epsilon>0$. Then $\exists$ $n$ such
%   that $1/n<\epsilon$. By construction of $A$ from open covers, there
%   exists $x_{nk} \in A$ such that
%   $x \in N_{1/n}(x_{nk}) \subseteq N_\epsilon(x_{nk})$. Hence $A$ is
%   dense.
% \end{solution}


% \begin{problem}
%   Let $X$ be a metric space with metric $d_x$. $A \subseteq X$ is
%   \textbf{disconnected} if $\exists$ open sets $U$ and $V$ such that
%   $A \subseteq (U \cup V)$, $A \cap U \cap V = \emptyset$,
%   $U \cap A \neq \emptyset$, and $V \cap A \neq \emptyset$. If $A$ is
%   not disconnected, then $A$ is \textbf{connected}.
%   \begin{enumerate}
%   \item Draw a picture of a disconnected set in $\R^2$ along with the
%     open sets $U$ and $V$ that satisfy the definition of disconnected. 
%   \item Show that $X$ is connected if and only if the only subsets of
%     $X$ that are both open and closed are $X$ and $\emptyset$.
%   \item Let $Y$ be another metric space with metric $d_y$. Show that
%     if $f:X \to Y$ is continuous, and $A$ is connected, then $f(A)$ is
%     connected. \emph{Hint: show the contrapositive, i.e. that if
%       $f$ is continuous and $f(A)$ is disconnected, then $A$ is
%       disconnected.}
%   \item Suppose $f:[a,b] \to \R$ is continuous and $f(a) < f(b)$.
%     Prove that for any $y \in [f(a),f(b)]$, $\exists x \in [a,b]$ such
%     that $f(x) = y$. \emph{Hint: use the result from the previous
%       part.}
%   \end{enumerate}
% \end{problem}
% \begin{solution}$\;$
%   \begin{centering}
%     \includegraphics[width=0.5\textwidth]{disconnected}
%   \end{centering}
%   \begin{enumerate}
%   \item The picture should look something like the above. The set $A$ is
%     shown in red. The dashed circles are $U$ and $V$. $U$ and $V$ can,
%     but need not overlap, as long as they overlap outside of $A$.
%   \item Suppose $U \subset X$ is open, closed, and not empty. Then
%     $V=U^c$ is also open and not empty. Furthermore,
%     $X \subseteq U \cup V$, and $X \cap U \cap V = \emptyset$, and $U
%     \cap X = U \neq \emptyset$, $V \cap X = V \neq
%     \emptyset$. Therefore $X$ is disconnected.  
   
%     Conversely, suppose $X$ is disconnected. Then $\exists U,V$, both
%     open and not empty, such
%     that $U \cap V = \emptyset$, $U \cup V = X$. But then $V = U^c$
%     and $U = V^c$, so $U$ and $V$ are both closed as well.
%   \item Suppose $f(A)$ is disconnected. Then $\exists$ open $U$, $V$,
%     such that $f(A) \subseteq (U \cup V)$ and $f(A) \cap U \cap V =
%     \emptyset$. Since $f$ is continuous, $f^{-1}(U)$ and $f^{-1}(V)$
%     are open. Also, $f(A) \subseteq (U \cup V)$ implies that $A
%     \subseteq f^{-1}(U) \cup f^{-1}(V)$. Furthermore, if $x \in A \cap
%     f^{-1}(U) \cap f^{-1}(V)$, then $f(x) \in f(A)$, $f(x) \in U$, and
%     $f(x) \in V$, so  $f(A) \cap U \cap V =
%     \emptyset$ implies $A \cap f^{-1}(U) \cap f^{-1}(V) = \emptyset$.
%     Finally, $f(A) \cap U \neq \emptyset$ implies $A \cap f^{-1}(U)
%     \neq \emptyset$ (and the same is true of $V$), so $A$ is
%     disconnected.
%   \item $[a,b]$ is connected and $f$ is continuous, so $f([a,b])$ must
%     also be connected. If $y \in (f(a),f(b))$, but
%     $y \not\in f([a,b])$, then $U = (-\infty,y)$, and $V = (y,\infty)$
%     would make $f([a,b])$ disconnected. Therefore,
%     $\exists x \in [a,b]$ such that $f(x) = y$. This result is the
%     called the intermediate value theorem, which you may have seen
%     before. The interesting part of proving it this way is that it
%     shows that the result of (iii) is one way to generalize the
%     intermediate value theorem to other contexts.
%   \end{enumerate}
% \end{solution}

% \bibliographystyle{jpe}
%\bibliography{../../526}

\end{document}
