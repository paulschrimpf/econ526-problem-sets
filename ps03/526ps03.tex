\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{enumitem}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{newpxtext}
\usepackage{newpxmath}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage{ulem}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 
\def\R{\mathbb{R}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{problem}{Problem}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\title{Problem Set 3}
\date{Due: Monday, September 30th}

\begin{document}

\maketitle

\begin{problem}
  Consider the problem
  \[ \max_{\{c_t\}_{t=0}^\infty, \{s_t\}_{t=1}^\infty} \sum_{t=0}^\infty \beta^t \log(c_t)
    \text{ s.t. } s_{t+1} = (1+r)(s_t - c_t) \]
  \begin{enumerate}
  \item State the Bellman equation for this problem.
  \item Solve for the value function.
  \item Now suppose that instead of the interest rate, $r$, being constant,
    the interest rate is random. Each period the $r$ is one of $K$
    values, $r_1, ..., r_k$.  Independently every period, $r = r_i$
    with probability $p_i$. The state variables are now $s$ and $r$,
    and the Bellman equation can be written as
    \[ V(r, s) = \max_{c,s'}\log(c) + \beta \sum_{i=1}^K p_i V(r_i, s')
      \text{ s.t. } s' = (1+r)(s - c) \]
    Solve for $V(r,s)$. {\slshape{Hint: a good guess for the
        functional form of the value function is $V(r_i,s) = A
        \log(s) + B(r_i)$. Use this guess and the Bellman equation to
        solve for $A$ and $B(r)$.}}
  \end{enumerate}
\end{problem}

\vspace{6pt}
\noindent\rule{\linewidth}{1pt}
\vspace{6pt}


\begin{problem}
  Suppose an unemployed worker receives a wage offer independently
  distributed uniformly on $[0,\bar{w}]$ each period. If the worker
  accepts the offer, the worker receives the wage forever. If the
  worker declines the offer, the worker receives unemployment benefits
  of $b$ and continues to be unemployed next period. The worker
  discounts the future at rate $\beta$, and has instantaneous utility
  equal to wages or unemployment benefits.
  \begin{enumerate}
  \item Find the utility from accepting a wage of $w$. \textit{Hint: this
      is short.}
  \item Write the Bellman equation for the value of receiving a wage
    offer of $w$. \\
    \textit{Hint: There are multiple ways to set this up. One way is
      to let $V(w)$ be the value of getting an offer of $w$. Given an
      offer of $w$, you can either accept it and 
      get your answer from the previous part, or reject it and get $b$
      plus the discount factor times the expectation of $V(w)$ next
      period, i.e.\ $\beta \int_{0}^{\bar{w}} V(w') \frac{1}{\bar{w}} dw'$ }
  \item Argue that the optimal strategy must be to accept all wages
    above some reservation wage $w^r$. \\
    \textit{Hint: the value of rejecting a wage offer does not depend
      on the offered wage.}
  \item Given the reservation wage strategy, explain why you can write
    the value of rejecting a wage offer as
    \[ v_0 = b + \beta \left(\int_{w^r}^{\bar{w}} \frac{w'}{1-\beta}
       \frac{1}{\bar{w}} dw' + \frac{w^r}{\bar{w}} v_0 \right). \]
  \item The reservation wage must be such that $ v_0 =
    \frac{w^r}{1-\beta}.$ Use this fact to calculate $\frac{\partial
      w^r}{\partial b}$.  
  \end{enumerate}
\end{problem}

%\bibliographystyle{jpe}
%\bibliography{../../526}

\end{document}
