\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{enumitem}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mdframed}
\usepackage{newpxmath}
\usepackage{newpxtext}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage{ulem}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 
\def\R{\mathbb{R}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{prob}{Problem}
\newtheorem{solution}{Solution to problem}
\newenvironment{problem}
  {\begin{mdframed}\begin{prob}}
      {\end{prob}\end{mdframed}}

\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}
\usepackage{natbib}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\newcommand{\norm}[1]{\left\Vert {#1} \right\Vert}


\title{Problem Set 4}
\date{Due: at TA tutorial on Monday, October 21st}

\begin{document}

\maketitle

\begin{problem}
  Prove that if the cardinality of $A$ is infinite, then $|A| = |A
  \times A|$.

  
  This problem is difficult. The proof uses Zorn's lemma (see
  below). We will encounter similar uses of Zorn's lemma at least two
  more times, so this will be good practice.

  Suggested steps: 
  \begin{enumerate}
  \item Review section 2.2 on order relations from the notes on sets.    
  \item Let
    \[
      X = \{(B,f) : B \subseteq A,\; f:B \to B \times B,\; f \text{ is
        bijective} \}.
    \]
    Define $(B,f) \preceq (D,g)$ if $B \subseteq D$ and $g(x) = f(x)$
    for all $x \in B$. Verify that $\preceq$ is a partial order on
    $X$.
  \item Show that $X$ is non-empty because there exists a countable
    subset of $A$ and there exists a bijection from
    $\mathbb{N} \to \mathbb{N} \times \mathbb{N}$.
  \item Let $C \subseteq X$ be a chain. Define
    $\overline{B} = \cup_{(B,f) \in C} B$ and $\overline{f}(x) = f(x)$
    for any $x \in B$ with $(B,f) \in C$.  Show that
    $(\overline{B}, \overline{f})$ is an upper bound for $C$.
  \item\label{max} Use Zorn's lemma to conclude that $C$ has a maximal element,
    $(B^*,f^*)$.
  \item\label{union} Let $B \subseteq A$. Show that if $|B| < |A|$,
    then $\exists D \subseteq A$ such that $|D| = |B|$ and $D \cap B =
    \emptyset$.  
  \item\label{extend} Show that if $f : B \to B \times B$ is
    bijective, $|D| = |B|$, and $D \cap B = \emptyset$, then there
    exists a bijection
    \[ g: (B \cup D) \to (B \cup D) \times (B \cup D) \]
    {\slshape{Hint: $(B \cup D) \times (B \cup D) = (B \times B) \cup
        (B\times D) \cup (D \times B) \cup (D \times D)$}}
  \item Use parts \ref{union} and \ref{extend} to argue that if $|B^*|
    < |A|$, then $(B^*, f^*)$ would not be maximal in $C$. Conclude
    $|B^*| = |A|$.
  \end{enumerate}
  I think part \ref{extend} is the hardest to show. You can answer the
  rest of the question without completing part \ref{extend}. 
  
  \paragraph{Definitions}
  \begin{itemize}
  \item A \textbf{partial order} as a relation that is
    \begin{enumerate}
    \item transitive $x \preceq y$ and $y \preceq z$ implies $x \preceq
      z$
    \item reflexive $x \preceq x$
    \item anti-symmetric $x \preceq y$ and $y \preceq x$ implies $x =
      y$
    \end{enumerate}
  \item A \textbf{chain} is subset of a partially ordered set where all
    elements are comparable. I.e. if $C \subseteq X$ is a chain, then
    for all $x, y \in C$, either $x \preceq y$ or $y \preceq x$.
  \item \textbf{Zorn's lemma} says that if $X$ is a partially ordered
    set and $C \subseteq X$ is a chain and $C$ has an upper bound
    (i.e. $\exists \overline{x} \in X$ such that $\overline{x} \succeq x$
    $\forall x \in C$), then $C$ has a maximal element, i.e.
    $\exists x^\ast \in C$ such that $x^\ast \succeq x$
    $\forall x \in C$.
  \end{itemize}    
\end{problem}

Like I posted on Canvas, the following is useful


\begin{lemma}\label{partition}
  If $|B|$ is infinite, then there exists a partition of B into
  countable subsets. I.e.
  $$
  B = \cup_{p \in P} B_p
  $$
  where each $B_p$ is countable, and the $B_p$ are disjoint.
\end{lemma}
\begin{proof}
  Let $X = \{$collections of disjoint countable subsets of
  $B\}$. $X$ is not empty since $B$ is infinite. Partially order $X$
  by 
  $$
  \{B_p\}_{p \in P} \preceq \{C_q\}_{q \in Q}
  $$
  if for all $p$, $B_p = C_q$ for some q. An upper bound for any chain
  is the collection of all subsets in the chain. By Zorn's lemma, the
  chain has a maximal element, $P$. Since $P$ is maximal,
  $$L = B \setminus U_{p \in P} B_p $$
  must be finite. We can then add elements of $L$ to any $B_p$ and
  then $P$ will be a partition. 
\end{proof}


\begin{lemma}\label{add}
  If B and D are infinite sets with $|B| = |D|$ and $B$ and $D$ are
  disjoint, then $|B \cup D| = |B|$.
\end{lemma}

The assumptions that $|B| = |D|$ and $B$ and $D$ are disjoint are just to
simplify the proof.

\begin{proof}
  By lemma \ref{partition}, we can partition $B$ in to countable subsets,
  $B_p$. Similarly, we can partition $D$ as
  $$
  D = \cup_{p \in P} D_p
  $$
  where  $D_p = \{f(b): b  \in B_p\}$ and $f: B \to D$ is a bijection.
  Therefore,
  $$
  B \cup D = \cup_{p \in P} (B_p \cup D_p)
  $$
  Finally, each $B_p$ and $D_p$ are countable, so there exists a bijection
  between them and $\mathbb{N}$, and between $B_p and (B_p U D_p)$
  defined by e.g.
  $$
  f(b_n) = \begin{cases}
    b_{(n+1)/2} &\text{if n odd} \\
    d_{n/2} & \text{if n even}
  \end{cases}
  $$
\end{proof}

\begin{solution}
  \begin{enumerate}
  \item
  \item $\preceq$ is reflexive, antisymmetric, and transitive because
    $\preceq$ is defined in terms of $=$ and $\subseteq$, and each of
    these are.
  \item If $A$ is infinite, then $A$ is at least countable, so there
    exists $\{a_i \}_{i=1}^\infty \subseteq A$. We can construct a
    bijection from $\mathbb{N}$ to $\mathbb{N} \times \mathbb{N}$ by
    e.g.
    \begin{align*}
      f(1) = & ( 1, 1)  \\
      f(2) = & ( 1, 2)  \\
      f(3) = & ( 2, 1)  \\
      f(4) = & ( 2, 2)  \\
      f(5) = & ( 1, 3)  \\
      f(6) = & ( 3, 1)  \\
      f(7) = & ( 3, 2)  \\
      \vdots & \vdots
    \end{align*}
  \item Clearly, $\overline{B} \supseteq B$ and $\overline{f}(x) =
    f(x)$ in $B$ for all $(B,f) \in C$, so $(\overline{B},
    \overline{f}) \succeq (B,f)$. We should also make sure that $(\overline{B},
    \overline{f}) \in X$. For this, we need $\overline{f}$ to be
    bijective. If $\overline{f}(x) = \overline{f}(z)$ for some $x , z
    \in \overline{B}$, then $\exists (B_x,f_x)$ and $(B_z,f_z)$ in the
    chain with $x\in B_x$ and $z \in B_z$. $B_x$ and $B_z$ are in a
    chain, so either $B_x \subseteq B_z$ or vice versa. For clear notation assume $B_x
    \subseteq B_z$. Then $x, z \in B_z$ and $f_z(x) = f_z(z)$. $f_z$
    is bijective by assumption, so this is only possible if $x =
    z$. Hence $\overline{f}(x) = \overline{f}(z)$ only if $x = z$,
    i.e. $\overline{f}$ is one to one.

    To show $\overline{f}$ is onto,suppose $(x,z) \in \overline{B}
    \times \overline{B}$. As above, $\exists$ $(B_x,f_x)$ and $(B_z,
    f_z)$ containing $x$ and $z$ respectively. Since we're in a chain,
    $x, z \in B_z$ (or $B_x$ but assume it's $B_z$ to keep the notation
    clear). $f_z: B_z \to B_z \times B_z$ is a bijection, so there
    exists $y \in B_z$ with $f_z(y) = (x,z)$. Moreover, by
    construction $\overline{f}(y) = f_z(y) = (x,z)$. Hence,
    $\overline{f}$ is surjective.
  \item Okay
  \item An implication of lemma \ref{add} is that
    $$ |B \cup (A \setminus B)| = \max\{|B|, |A \setminus B|\} $$
    Hence if $|A| > |B|$, we must have $|A \setminus B| > |B|$, and we
    can choose $D \subseteq A \setminus B$ with $|D| = |B|$. 
  \item By assumption, $\exists$ a bijection $f: B \to B \times B$ and
    $h:B \to D$. Then $b \to (f_1(b), h(f_2(b)))$ gives a bijection
    from $B \to B \times D$. Similarly, we can construction bijections
    to $D \times D$ and $D \times B$. From lemma \ref{add} there
    exists a bijection from $B$ to $B \cup B'$ where $|B'| = |B|$. We
    can then construction bijections
    \[ B \cup D \to (B \cup D) \cup (B' \cup D') \to (B \times B) \cup
      (B\times D) \cup (D \times B) \cup (D \times D) \]
    where the first $\to$ come from lemma \ref{add}, and the second is
    from the bijections from $B \to B \times B$, $D \to B \times D$,
    $B' \to D \times B$ and $D' \to D \times D$.
  \item The previous two parts showed that if $|B^*| < |A|$, then we
    can find $((B^* \cup D), g) \succ (B^*, f^*)$. This contradicts
    $B^*, f^*$ being maximal, so $|B^*| = |A|$.
  \end{enumerate}
\end{solution}


% \begin{problem}
%   Let $X$ be a metric space with metric $d_X$ and $Y$ be a
%   \emph{compact} metric space with metric $d_Y$. 
%   Define a metric on the product space $X \times Y =
%   \{(x,y): x \in X, y \in Y \}$ by 
%   \[ 
%   d\left( (x_1,y_1), (x_2, y_2) \right) = d_x(x_1,x_2) + d_y(y_1,y_2)
%   \]
%   \begin{enumerate}
%   \item Show that if $f:X \to Y$ is continuous then
%     \[
%     \mathrm{graph}(f) = \{ (x,y) : y=f(x) , x \in X \}
%     \]
%     is a closed subset of $X \times Y$. 
%   \item Show that if $\mathrm{graph}(f)$ is closed, then $f$ is
%     continuous.
%   \item Which, if either, of the previous two parts required $Y$ to be
%     compact?  
%   \end{enumerate}
% \end{problem}

% \par
% \noindent\rule{\linewidth}{1pt} 
% \par

% \begin{problem}
%   Let $V$ be a vector space. A set $S \subseteq V$ is \textbf{affine}
%   if for all $\alpha \in \R$ and all $\mathbf{x}, \mathbf{y} \in S$, 
%   \[ \alpha \mathbf{x} + (1-\alpha) \mathbf{y} \in S. \]
%   \begin{enumerate}
%   \item Prove that a linear subspace is affine.
%   \item Prove that an affine set, $S$, is a linear subspace if $0 \in
%     S$
%   \item Let $S$ and $T$ be affine. We say that $S$ is parallel to $T$
%     if $\exists \mathbf{x} \in V$ such that $S = \{ \mathbf{x} +
%     \mathbf{y} : \mathbf{y} \in T\}$. Show that the relation $S$ is
%     parallel to $T$ is an equivalence relation among affine sets.
%   \end{enumerate}
% \end{problem}

% \par
% \noindent\rule{\linewidth}{1pt} 
% \par

% \begin{problem}
%   Let $V$ be a vector space. A set $S \subseteq V$ is \textbf{convex}
%   if for all $\alpha \in [0,1]$ and all $\mathbf{x}, \mathbf{y} \in S$, 
%   \[ \alpha \mathbf{x} + (1-\alpha) \mathbf{y} \in S. \]
%   \begin{enumerate}
%   \item Show that if $S$ and $T$ are convex, then so is $S \cap T$.
%   \item Show that if $S$ and $T$ are convex, then so is $S + T$.
%   \end{enumerate}
% \end{problem}

% \par
% \noindent\rule{\linewidth}{1pt} 
% \par

% \begin{problem}\label{contLin}
%   Let $V$ and $W$ be normed vector spaces and $A: V \to W$ be
%   linear. We say $A$ is \textbf{continuous} at $x$, if for every $\epsilon>0$
%   $\exists \delta >0$ such that for all $y \in V$ with $\norm{x-y} <
%   \delta$ we have $\norm{A x - Ay } < \epsilon$.
%   \begin{enumerate}
%   \item Prove that if $A$ is continuous at $0$, then $A$ is continuous
%     at all $v \in V$. 
%   \item $A$ is \textbf{bounded} if there exists a constant $M$ such
%     that
%     \[ \norm{A v} \leq M \norm{v} \] 
%     for all $v \in V$. Prove that $A$ is continuous (at all $v \in V$)
%     if and only if $A$ is bounded.
%   \item Show that if $V$ is finite dimensional, then $A$ is
%     continuous.
%   \item Give an example of a discontinuous linear transformation. 
%   \end{enumerate}  
% \end{problem}

%\newpage 

% \begin{problem}
%   Recall that $L(V,W)$ is the vector space of all linear
%   transformations from $V$ to $W$. Let $\norm{v}_V$ denote the norm on
%   $V$ and $\norm{w}_W$ denote the norm on $W$. We say that $A \in
%   L(V,W)$ is bounded if there exists $C_A \geq 0$ such that
%   $\norm{Av}_W \leq C_A \norm{v}_V$ for all $v \in V$.  Let $BL(V,W)$
%   denote the set of all bounded linear transformations from $V$ to
%   $W$.
%   \begin{enumerate}
%   \item Show that $BL(V,W)$ is a linear subspace.
%   \item Show that the following is a norm on $BL(V,W)$,
%     \[ \norm{A} = \sup_{v \in V: v \neq 0} \frac{\norm{A
%         v}_{W}}{\norm{v}_V} \]
%   \item Suppose $A_n \to A$ in $BL(V,W)$ and $v_n \to v$ in $V$. Show
%     that $A_n v_n \to A v$ in $W$. [Hint: a useful fact is that
%     $\norm{A}$ is defined such that $\norm{Av}_W \leq \norm{A}
%     \norm{v}_V$ for all $v \in V$.]
%   \end{enumerate}
% \end{problem}

% \noindent\rule{\linewidth}{1pt} 
% \par

% \begin{problem}
%   Let $A$ be an $m$ by $n$ matrix and $b \in \R^m$. The goal of this
%   problem is prove Farkas' Lemma, which states that either:
%   \begin{itemize}
%   \item[(i)] $\exists x = (x_1, ..., x_n) \in \R^n$ such that $x_1
%     \geq 0, ..., x_n \geq 0$ and $Ax = b$ or
%   \item[(ii)] $\exists y \in \R^m$ such that $y^T A e_i \geq 0$ for
%     each standard basis vector $e_i \in \R^n$ and $y^T b < 0$
%   \end{itemize}
%   but not both.
  
%   \begin{enumerate}
%   \item Show that if (i), then not (ii). 
%   \item Use the separating hyperplane theorem to show that not (i)
%     implies (ii).
%   \end{enumerate}
% \end{problem}


%\bibliographystyle{jpe}
%\bibliography{../../526}

\end{document}
