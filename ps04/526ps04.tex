\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{enumitem}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{newpxmath}
\usepackage{newpxtext}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage{ulem}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 
\def\R{\mathbb{R}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{problem}{Problem}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}
\usepackage{natbib}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\newcommand{\norm}[1]{\left\Vert {#1} \right\Vert}


\title{Problem Set 4}
\date{Due: at TA tutorial on Monday, October 21st}

\begin{document}

\maketitle

\begin{problem}
  Prove that if the cardinality of $A$ is infinite, then $|A| = |A
  \times A|$.

  
  This problem is difficult. The proof uses Zorn's lemma (see
  below). We will encounter similar uses of Zorn's lemma at least two
  more times, so this will be good practice.

  Suggested steps: 
  \begin{enumerate}
  \item Review section 2.2 on order relations from the notes on sets.    
  \item Let
    \[
      X = \{(B,f) : B \subseteq A,\; f:B \to B \times B,\; f \text{ is
        bijective} \}.
    \]
    Define $(B,f) \preceq (D,g)$ if $B \subseteq D$ and $g(x) = f(x)$
    for all $x \in B$. Verify that $\preceq$ is a partial order on
    $X$.
  \item Show that $X$ is non-empty because there exists a countable
    subset of $A$ and there exists a bijection from
    $\mathbb{N} \to \mathbb{N} \times \mathbb{N}$.
  \item Let $C \subseteq X$ be a chain. Define
    $\overline{B} = \cup_{(B,f) \in C} B$ and $\overline{f}(x) = f(x)$
    for any $x \in B$ with $(B,f) \in C$.  Show that
    $(\overline{B}, \overline{f})$ is an upper bound for $C$.
  \item\label{max} Use Zorn's lemma to conclude that $C$ has a maximal element,
    $(B^*,f^*)$.
  \item\label{union} Let $B \subseteq A$. Show that if $|B| < |A|$,
    then $\exists D \subseteq A$ such that $|D| = |B|$ and $D \cap B =
    \emptyset$.  
  \item\label{extend} Show that if $f : B \to B \times B$ is
    bijective, $|D| = |B|$, and $D \cap B = \emptyset$, then there
    exists a bijection
    \[ g: (B \cup D) \to (B \cup D) \times (B \cup D) \]
    {\slshape{Hint: $(B \cup D) \times (B \cup D) = (B \times B) \cup
        (B\times D) \cup (D \times B) \cup (D \times D)$}}
  \item Use parts \ref{union} and \ref{extend} to argue that if $|B^*|
    < |A|$, then $(B^*, f^*)$ would not be maximal in $C$. Conclude
    $|B^*| = |A|$.
  \end{enumerate}
  I think part \ref{extend} is the hardest to show. You can answer the
  rest of the question without completing part \ref{extend}. 
  
  \paragraph{Definitions}
  \begin{itemize}
  \item A \textbf{partial order} as a relation that is
    \begin{enumerate}
    \item transitive $x \preceq y$ and $y \preceq z$ implies $x \preceq
      z$
    \item reflexive $x \preceq x$
    \item anti-symmetric $x \preceq y$ and $y \preceq x$ implies $x =
      y$
    \end{enumerate}
  \item A \textbf{chain} is subset of a partially ordered set where all
    elements are comparable. I.e. if $C \subseteq X$ is a chain, then
    for all $x, y \in C$, either $x \preceq y$ or $y \preceq x$.
  \item \textbf{Zorn's lemma} says that if $X$ is a partially ordered
    set and $C \subseteq X$ is a chain and $C$ has an upper bound
    (i.e. $\exists \overline{x} \in X$ such that $\overline{x} \succeq x$
    $\forall x \in C$), then $C$ has a maximal element, i.e.
    $\exists x^\ast \in C$ such that $x^\ast \succeq x$
    $\forall x \in C$.
  \end{itemize}    
\end{problem}


% \begin{problem}
%   Let $X$ be a metric space with metric $d_X$ and $Y$ be a
%   \emph{compact} metric space with metric $d_Y$. 
%   Define a metric on the product space $X \times Y =
%   \{(x,y): x \in X, y \in Y \}$ by 
%   \[ 
%   d\left( (x_1,y_1), (x_2, y_2) \right) = d_x(x_1,x_2) + d_y(y_1,y_2)
%   \]
%   \begin{enumerate}
%   \item Show that if $f:X \to Y$ is continuous then
%     \[
%     \mathrm{graph}(f) = \{ (x,y) : y=f(x) , x \in X \}
%     \]
%     is a closed subset of $X \times Y$. 
%   \item Show that if $\mathrm{graph}(f)$ is closed, then $f$ is
%     continuous.
%   \item Which, if either, of the previous two parts required $Y$ to be
%     compact?  
%   \end{enumerate}
% \end{problem}

% \par
% \noindent\rule{\linewidth}{1pt} 
% \par

% \begin{problem}
%   Let $V$ be a vector space. A set $S \subseteq V$ is \textbf{affine}
%   if for all $\alpha \in \R$ and all $\mathbf{x}, \mathbf{y} \in S$, 
%   \[ \alpha \mathbf{x} + (1-\alpha) \mathbf{y} \in S. \]
%   \begin{enumerate}
%   \item Prove that a linear subspace is affine.
%   \item Prove that an affine set, $S$, is a linear subspace if $0 \in
%     S$
%   \item Let $S$ and $T$ be affine. We say that $S$ is parallel to $T$
%     if $\exists \mathbf{x} \in V$ such that $S = \{ \mathbf{x} +
%     \mathbf{y} : \mathbf{y} \in T\}$. Show that the relation $S$ is
%     parallel to $T$ is an equivalence relation among affine sets.
%   \end{enumerate}
% \end{problem}

% \par
% \noindent\rule{\linewidth}{1pt} 
% \par

% \begin{problem}
%   Let $V$ be a vector space. A set $S \subseteq V$ is \textbf{convex}
%   if for all $\alpha \in [0,1]$ and all $\mathbf{x}, \mathbf{y} \in S$, 
%   \[ \alpha \mathbf{x} + (1-\alpha) \mathbf{y} \in S. \]
%   \begin{enumerate}
%   \item Show that if $S$ and $T$ are convex, then so is $S \cap T$.
%   \item Show that if $S$ and $T$ are convex, then so is $S + T$.
%   \end{enumerate}
% \end{problem}

% \par
% \noindent\rule{\linewidth}{1pt} 
% \par

% \begin{problem}\label{contLin}
%   Let $V$ and $W$ be normed vector spaces and $A: V \to W$ be
%   linear. We say $A$ is \textbf{continuous} at $x$, if for every $\epsilon>0$
%   $\exists \delta >0$ such that for all $y \in V$ with $\norm{x-y} <
%   \delta$ we have $\norm{A x - Ay } < \epsilon$.
%   \begin{enumerate}
%   \item Prove that if $A$ is continuous at $0$, then $A$ is continuous
%     at all $v \in V$. 
%   \item $A$ is \textbf{bounded} if there exists a constant $M$ such
%     that
%     \[ \norm{A v} \leq M \norm{v} \] 
%     for all $v \in V$. Prove that $A$ is continuous (at all $v \in V$)
%     if and only if $A$ is bounded.
%   \item Show that if $V$ is finite dimensional, then $A$ is
%     continuous.
%   \item Give an example of a discontinuous linear transformation. 
%   \end{enumerate}  
% \end{problem}

%\newpage 

% \begin{problem}
%   Recall that $L(V,W)$ is the vector space of all linear
%   transformations from $V$ to $W$. Let $\norm{v}_V$ denote the norm on
%   $V$ and $\norm{w}_W$ denote the norm on $W$. We say that $A \in
%   L(V,W)$ is bounded if there exists $C_A \geq 0$ such that
%   $\norm{Av}_W \leq C_A \norm{v}_V$ for all $v \in V$.  Let $BL(V,W)$
%   denote the set of all bounded linear transformations from $V$ to
%   $W$.
%   \begin{enumerate}
%   \item Show that $BL(V,W)$ is a linear subspace.
%   \item Show that the following is a norm on $BL(V,W)$,
%     \[ \norm{A} = \sup_{v \in V: v \neq 0} \frac{\norm{A
%         v}_{W}}{\norm{v}_V} \]
%   \item Suppose $A_n \to A$ in $BL(V,W)$ and $v_n \to v$ in $V$. Show
%     that $A_n v_n \to A v$ in $W$. [Hint: a useful fact is that
%     $\norm{A}$ is defined such that $\norm{Av}_W \leq \norm{A}
%     \norm{v}_V$ for all $v \in V$.]
%   \end{enumerate}
% \end{problem}

% \noindent\rule{\linewidth}{1pt} 
% \par

% \begin{problem}
%   Let $A$ be an $m$ by $n$ matrix and $b \in \R^m$. The goal of this
%   problem is prove Farkas' Lemma, which states that either:
%   \begin{itemize}
%   \item[(i)] $\exists x = (x_1, ..., x_n) \in \R^n$ such that $x_1
%     \geq 0, ..., x_n \geq 0$ and $Ax = b$ or
%   \item[(ii)] $\exists y \in \R^m$ such that $y^T A e_i \geq 0$ for
%     each standard basis vector $e_i \in \R^n$ and $y^T b < 0$
%   \end{itemize}
%   but not both.
  
%   \begin{enumerate}
%   \item Show that if (i), then not (ii). 
%   \item Use the separating hyperplane theorem to show that not (i)
%     implies (ii).
%   \end{enumerate}
% \end{problem}


%\bibliographystyle{jpe}
%\bibliography{../../526}

\end{document}
