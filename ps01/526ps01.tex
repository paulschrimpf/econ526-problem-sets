\documentclass[11pt,reqno]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
%\usepackage[small,compact]{titlesec} 

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage{ulem}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{problem}{Problem}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}

\linespread{1.1}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{} 
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
\fancyhf{} % clear all header and footer fields
\fancyfoot[C]{\small{\thepage}} % except the center
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    \rule{\linewidth}{1pt} 
    {\Large \textbf{\textsc{\@title}}} \par
    {\normalsize \textsc{Paul Schrimpf}} \par
    {\normalsize \textsc{\@date}} \par
    {\small \textsc{University of British Columbia}} \par
    {\small \textsc{Economics 526}} \par
    \rule{\linewidth}{1pt} 
  \end{center}%
  \par \vskip 0.9em
}
\makeatother

\title{Problem Set 1}
\date{Due: Monday, September 16th}

\begin{document}

\maketitle

\begin{problem}[\cite{dixit1990} exercise 2.2]
  A consumer can buy two goods, $x$ and $y$, at prices $p$ and
  $q$. She has income $I$, and her utility function is
  \begin{align*}
    U(x,y) = \alpha \log(x - x_0) + \beta \log (y - y_0)
  \end{align*}
  where $\alpha$, $\beta$, $x_0$, and $y_0$ are some constants and
  $\alpha + \beta = 1$. Show that the optimal expenditures on the two
  goods (i.e. $px^\ast$ and $qy^\ast$) are linear functions of income
  and prices.
\end{problem}

\begin{problem}[\cite{dixit1990} exercise 3.2] 
  There is $Y$ units of a good available. There are two consumers who
  envy one another. Let $y_1$ and $y_2$ be the amount of the good
  given to consumer $1$ and $2$. Consumer $1$'s utility function is
  \[ u_1(y_1,y_2) = y_1 - k y_2^2 \]
  and consumer $2$'s utility function is
  \[ u_2(y_1,y_2) = y_2 - k y_1^2. \]
  You want to maximize the sum of their utilities subject to $y_1 \geq
  0$, $y_2 \geq 0$ and $y_1 + y_2 \leq Y$. Show that if $Y > 1/k$,
  then the resource constraint will be slack at the optimum. Interpret
  the result.
\end{problem} 

\begin{problem}[Contracting with asymmetric information]
  \footnote{This problem is based on chapter 2 of \textit{Contract
      theory} by Bolton and Dewatripont. Feel free to consult that
    book if you are having difficulty.} 
  Consider a monopolist selling
  a good to two types of consumers, high 
  demand and low demand. High types get utility 
  \[ \theta_h v(q) - T \]
  from consuming $q$ units of the good at cost $T$, where
  $v(q)>0$ and $v'(q)>0$. Similarly, low types get utility 
  \[ \theta_l v(q) - T \]
  where $\theta_l < \theta_h$. 
  The portion of low types in the population is $\beta$ and the
  portion of high types is $1-\beta$. Assume that both types of
  consumer get utility $0$ from not consuming. Also assume that
  there is no secondary market for the good. That is, consumers can only
  buy the good from the monopolist; they cannot trade amongst
  themselves. It costs the seller $cq$ to produce $q$ units of the
  good. 
  \begin{enumerate}
  \item\label{fb}\textbf{[First best]} Suppose the seller observes the types of
    the consumer and so 
    can charge them different prices. The seller maximizes profits subject
    to the consumers actually buying the good.
    \begin{align*}
      \max_{q_l,T_l,q_h,T_h} \beta(T_l - c q_l) + (1-\beta)(T_h - c
      q_h) \text{ s.t. } & \theta_l v(q_l) - T_l \geq 0 \\
      & \theta_h v(q_h) - T_h \geq 0
    \end{align*}
    write down the first order condition.
  \item\textbf{[Linear pricing]} Now suppose the seller cannot observe
    the types of the 
    consumers. Suppose the seller charges a constant linear price,
    $p$, so $T_l = p q_l$ and $T_h = p q_h$. Let $D_l(p)$ be the
    demand function for low types and $D_h(p)$ be the demand function
    for high types. In other words, 
    \[ D_i(p) = \argmax_q \theta_i v(q) - p q. \]
    Assume that $D_h'(p)<0$.
    \begin{enumerate}
    \item Write the firm's and consumer's first order conditions for
      $p$ and $q$ respectively. 
    \item Show that consumption is lower here than in part (\ref{fb}). 
    \item Let $S(q,T) = \beta(\theta_l v(q_l) - cq_l) + (1-\beta)
      (\theta_h v (q_h) - c q_h)$ be the total surplus. Show that the
      total surplus is lower here than in \ref{fb}.
    \end{enumerate}
  % \item\textbf{[Two-part tariffs]} 
  %   %\textit{You do not have to hand in your answer to this part.} \\
  %   Suppose the seller charges a two-part tariff
  %   $T(q) = z + pq$. Assume that $z$ is small relative to consumer
  %   income, so there is no income effect and demand only depends on
  %   $p$ provided utility from consuming and paying $z + D(p) p$
  %   exceeds utility from not consuming. Then the profit maximization
  %   problem assuming both types of consumers purchase something is:
  %   \begin{align*}
  %     \max_{z,p} \beta\left(z+pD_l(p) - c D_l(p)\right) + (1-\beta)
  %     \left( z + p D_h(p) - c D_h(p) \right) \text{ s.t. } & \theta_l
  %     v(D_l(p)) - (z + p D_l(p)) \geq 0 \\
  %     & \theta_h v(D_h(p)) - (z + p D_h(p)) \geq 0 \\
  %   \end{align*}
  %   \begin{enumerate}
  %   \item Use the envelope theorem on 
  %     \[ \max_q \theta_i v(q) - (z+pq) \] to show that
  %     $\theta_h v(D_h(p)) - (z + p D_h(p)) > \theta_l v(D_l(p)) - (z +
  %     p D_l(p))$.  [\slshape{Hint: what does the envelope theorem tell
  %       you about $u^*(\theta) = \max_q \theta v(q) - (z + pq)$?}]
  %     This implies that only the first constraint in the seller's
  %     problem can be binding.
  %   \item Write the seller's first order condition. Use the fact that
  %     only the first constraint binds to show that $p^*>c$ and there
  %     again consumption is lower than in (\ref{fb}).
  %   \end{enumerate}
  \item\textbf{[Second-best pricing]} Now suppose the seller can offer
    any pricing schedule, $T(q)$. This could be any function, perhaps
    very complicated. However, an important insight called the
    revelation principle is that the only thing that matters about
    $T(q)$ is $T_l=T(q_l)$ and $T_h = T(q_h)$ where $q_l$ and $q_h$
    are the quantities actually chosen by types $l$ and $h$. Thus, we
    can just focus on these two quantities
    \begin{align} 
      \max_{T_i,q_i} \beta(T_l - cq_l) +(1-\beta)(T_h - cq_h) 
      \text{s.t.} & \\
      \theta_h v(q_h) - T_h \geq & \theta_h v(q_l) - T_l \label{ich}
      \\
      \theta_l v(q_l) - T_l \geq & \theta_l v(q_h) - T_h \label{icl}
      \\
      \theta_h v(q_h) - T_h \geq & 0 \label{irh} \\
      \theta_l v(q_l) - T_l \geq & 0 \label{irl}
    \end{align}
    Constraints (\ref{ich}) and (\ref{icl}) are called incentive
    compatibility constraints. They say that type $i$ must prefer the
    contract designed for type $i$ to the contract for the other
    type. Constraints (\ref{irh}) and (\ref{irl}) are called
    individual rationality or participation constraints. They simply
    say that type $i$ should prefer buying the good to not buying it.
    \begin{enumerate}
    \item Argue that \ref{irh} will not bind because it is implied by
      \ref{irl} and \ref{ich}
    \item Guess that \ref{icl} also does not bind at the
      maximum. Write down the first order conditions and verify that 
      \ref{icl} indeed does not bind. 
    \item Show that $q_h$ is the same as in (\ref{fb}), but $q_l$ is
      lower. 
    \end{enumerate}     
  % \item The basic structure of this problem can be applied to many
  %   other settings such as credit rationing and income
  %   taxation. Explain what $\theta$, $v$, $q$ and $T$ might be
  %   interpreted as in one of these other settings.
  \end{enumerate}
\end{problem}

% \begin{problem}
%   Consider a simple static labor supply model. There is a single
%   representative consumer whose utility depends on consumption $c$ and
%   labor $l$. The price of consumption is $p$ and the wage is
%   $w$. Labor income is taxed at $\tau$. The consumer receives a lump
%   sum transfer from the government $T$. The consumer's problem is
%   \[ \max_{c,l} u(c,l) \text{ s.t. } pc \leq (1-\tau) wl + T \]
%   or substituting in the constraint,
%   \[ \max_{l} u\left(\frac{1}{p} (1-\tau) wl + T, l \right) \]
%   There is a single representative firm that produces $c = f(l)$. The
%   firm's profit maximization problem is 
%   \[ \max_l p f(l) - wl \]
%   \begin{enumerate}
%   \item Comment on the likely signs of $\frac{\partial u}{\partial
%       c}$, $\frac{\partial u}{\partial l}$ and $\frac{\partial
%       f}{\partial l}$. 
%   \item State the first order conditions for the consumer and firm.
%   \item Use the two first order conditions to calculate
%     $\frac{\partial l}{\partial T}$, $\frac{\partial l}{\partial
%       \tau}$, $\frac{\partial (w/p)}{\partial T}$, and
%     $\frac{\partial (w/p)}{\partial \tau}$. Comment on the signs of
%     these derivatives.
%   % \item Calculate the derivatives of consumption and utility with
%   %   respect the tax rate and the lump sum transfer.
%   % \item Suppose the government budget is required to balance, so $T/p =
%   %   \tau (w/p) l$. Find the derivatives of labor, consumption, and
%   %   utility with respect to the tax rate.  
%   \end{enumerate}
% \end{problem}

\bibliographystyle{jpe}
\bibliography{../../526}

\end{document}
